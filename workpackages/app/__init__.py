import os
from flask import Flask
from config import OIDC_CONFIG
from flask_login import LoginManager
from werkzeug.contrib.fixers import ProxyFix
from flask_oidc import OpenIDConnect
import json
import logging
logging.basicConfig()

basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)
app.wsgi_app = ProxyFix(app.wsgi_app)

app.config.from_object('config')

lm = LoginManager()
lm.init_app(app)
lm.login_view = 'login'

"""
from authomatic import Authomatic
authomatic = Authomatic(CONFIG, SECRET_KEY)
"""

oidc_overrides = {}
app.config.update(OIDC_CONFIG)
oidc = OpenIDConnect(app, **oidc_overrides)

from app import views

from app.workpackages import views as workpackageviews
from app.people import views as peopleviews
from app.export import views as exportviews
from app.allocations import views as allocationviews
from app.admin import views as adminviews
from app.reports import views as reportviews

app.register_blueprint(adminviews.adminviews, url_prefix="/admin")
app.register_blueprint(workpackageviews.workpackageviews, url_prefix="/workpackages")
app.register_blueprint(peopleviews.peopleviews, url_prefix="/people")
app.register_blueprint(exportviews.exportviews, url_prefix="/export")
app.register_blueprint(allocationviews.allocationviews, url_prefix="/allocations")
app.register_blueprint(reportviews.reportviews, url_prefix="/reports")

