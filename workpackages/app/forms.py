from flask_wtf import Form

from wtforms import TextField, BooleanField, TextAreaField
from wtforms.validators import Required, Length, Email
from flask_wtf.html5 import EmailField

class LoginForm(Form):
    openid = TextField('openid', validators = [Required()])
    remember_me = BooleanField('remember_me', default = False)

class EditForm(Form):
    nickname = TextField('nickname', validators = [Required()])
    about_me = TextAreaField('about_me', validators = [Length(min = 0, max = 140)])

class NewUserForm(Form):
    name = TextField('Full Name (Please use Sentence Case)', validators = [Required(), \
        Length(min = 0, max = 255)])
    email = EmailField('Work Email', validators = [Required(), Email()])
    jobtitle = TextField('Job Title', validators = [Required(), \
        Length(min = 0, max = 140)])
    # programme = 

