from hashlib import md5


class User():

    @staticmethod
    def map_from(user):
        """
        Maps user object to rethinkdb type
        """
        return dict(
            authid=user.authid,
            authprovider=user.authprovider,
            email=user.email,
            emailauth=user.emailauth,
            gender=user.gender,
            jobtitle=user.jobtitle,
            lastlogin=user.lastlogin,
            name=user.name,
            nickname=user.nickname,
            programme=user.programme,
            role=user.role,
            status=user.status,
	    lead=user.lead)

    @staticmethod
    def map_to(db_user):
        """
        Maps a rethinkdb user to the object
        todo: this needs to be refactored to be generic model binding
        """
        if db_user is None:
            return None
        
        return User(
            id=db_user['id'],
            authid=db_user.get('authid', ""),
            authprovider=db_user.get('authprovider', ""),
            email=db_user.get('email', ""),
            emailauth=db_user.get('emailauth', ""),
            gender=db_user.get('gender', ""),
            jobtitle=db_user.get('jobtitle', ""),
            lastlogin=db_user.get('lastlogin', ""),
            name=db_user.get('name', ""),
            nickname=db_user.get('nickname', ""),
            programme=db_user.get('programme', ""),
            role=db_user.get('role', ""),
            status=db_user.get('status', ""),
	    lead=db_user.get('lead', ""))

    def __init__(self, name, nickname="", authid="", authprovider="", \
        email="", emailauth="", gender="", lastlogin="", \
        programme="", role="", status="", jobtitle="", lead="", id=""):

        self.id = id
        self.authid = authid
        self.authprovider = authprovider
        self.email = email
        self.emailauth = emailauth
        self.gender = gender
        self.jobtitle = jobtitle
        self.lastlogin = lastlogin
        self.name = name
        self.nickname = nickname
        self.programme = programme
        self.role = role
        self.status = status
	self.lead = lead

    @staticmethod
    def avatar(size, email):
        return 'http://www.gravatar.com/avatar/' + md5(email).hexdigest() + '?d=mm&s=' + str(size)

    def is_authenticated(self):
        return True

    def is_admin(self):
        return (self.role == "admin" or self.role == "Admin")

    def is_lead(self):
        return (self.lead == "True")

    def is_active(self):
        return self.status == "Active"

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def __repr__(self):
        return '<User %r>' % (self.nickname)
