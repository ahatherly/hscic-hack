"""
View logic for workpackages
"""
from flask import Blueprint, render_template, request, g, jsonify, make_response, url_for, redirect, Response
from flask_login import current_user
from app.datastores.packages import PackagesStore
from app.datastores.people import PeopleStore
from app.datastores.portfolio import PortfolioStore
from app.datastores.deliverables import DeliverablesStore
from app import app, lm, html2text
from app.utils import *
from datetime import date
from copy import deepcopy
import json
from HTMLParser import HTMLParser

workpackageviews = Blueprint('workpackageviews', __name__,
                        template_folder='templates')

@workpackageviews.before_request
def before_request():
    if not current_user.is_authenticated:
        return lm.unauthorized()
    g.packages = PackagesStore(g.rdb_conn)
    g.people = PeopleStore(g.rdb_conn)
    g.portfolio = PortfolioStore(g.rdb_conn)
    g.deliverables = DeliverablesStore(g.rdb_conn)

@workpackageviews.route('/')
def list_workpackages_html():
    
    """
    Get WP list with appropriate filters applied
    """
    result = list_workpackages()
    
    """
    Get Lists for drop-downs
    """
    field_names = ['status', 'portfolio', 'programme', 'areaOfWork', 'serviceType', 'level1Type', 'level2Type']
    dq_filters = ['unallocated','missing_dates','past_end_date','before_start_date','no_programme','no_activity','under_allocated','over_allocated']
    value_lists = {}
    for field_name in field_names:
        value_lists[field_name] = g.packages.get_list(field_name)
    value_lists['dq_filters'] = dq_filters
    
    return render_template('workpackages/panel-workpackages43.html', \
        list=result['workpackages'], value_lists=value_lists, \
                query_vals=result['query_vals'])

@workpackageviews.route('/json/')
def list_workpackages_json():
    return jsonify(list=list_workpackages()['workpackages'])

@workpackageviews.route('/csv/')
def list_workpackages_csv():
    
    data = list_workpackages()['workpackages']
    #print data
    csv = ""

    for k in FIELD_LIST['csv_text']:
        csv = csv + '"' + k + '",'
    csv = csv + 'allocated,'
    for k in FIELD_LIST['csv_html']:
        csv = csv + '"' + k + '",'
    csv = csv + '\n'
    
    for row in data:
        for k in FIELD_LIST['csv_text']:
            if k in row:
                val = row[k]
                if type(val) is unicode:
                    val = removeNonAscii(val)
                else:
                    val = str(val)
                val = val.strip()
                val = val.replace('"', '\'')
                csv = csv + '"' + val + '",'
            else:
                csv = csv + ','
        
        # Get list of allocated people
        names = ""
        allocs = row['Assigned']
        for alloc in allocs:
            if alloc['Status'] == 'Active':
                if names != "":
                    names = names + '; '
                names = names + alloc['Name']
        csv = csv + '"' + names + '",'
        
        for k in FIELD_LIST['csv_html']:
            if k in row:
                val = row[k]
                if type(val) is unicode:
                    val = removeNonAscii(val)
                else:
                    val = str(val)
                # Convert from HTML to text (markdown)
                val = html2text.html2text(val)
                val = val.strip()
                val = val.replace('"', '\'')
                csv = csv + '"' + val + '",'
            else:
                csv = csv + ','
        csv = csv + '\n'
    
    #return Response(generate(data), mimetype='text/csv')
    headers = {}
    headers['Content-Disposition']='filename="WorkPackages.csv"'
    return Response(csv, mimetype='text/csv', headers=headers)

def list_workpackages():
    """ 
    Show the list of all work packages
    """
    filtered_args = filter_request_args(request.args, FIELD_LIST['query'])
    dq_filter = ""
    filter = {}
    for filterfield in filtered_args:
        val = request.args.get(filterfield, '')
        if len(val) > 0:
            if filterfield == 'dq_filter':
                dq_filter = val
            else:
                filter[filterfield] = val
    
    workpackages = g.packages.get_wp_list(filter, dq_filter)
    
    return dict(workpackages=workpackages, query_vals=filtered_args)

@workpackageviews.route('/add')
@workpackageviews.route('/add/')
def workpackage_add():
    """
    Add barebones workpackage
    """
    return render_template('workpackages/panel-new-workpackage43.html')

@workpackageviews.route('/check', methods=['POST','GET'])
@workpackageviews.route('/check/', methods=['POST','GET'])
def workpackage_check_duplicates():
    # retrieve form data
    #print request.args
    filtered_args = filter_request_args(request.args, FIELD_LIST['new'])
    name = filtered_args['name']
    """
    Check if a work package with this name already exists (issue #10)
    """
    duplicateWP = list(g.packages.get_workpackage_by_name(name))

    if len(duplicateWP) > 0:
        return "DUPLICATE"
    else:
        return "OK"

@workpackageviews.route('/additem')
@workpackageviews.route('/additem/')
def workpackage_add_workpackage():
    # retrieve form data
    filtered_args = filter_request_args(request.args, FIELD_LIST['new'])
    name = filtered_args['name']
        
    newworkpackage = {}
    newworkpackage['Assigned'] = []
    newworkpackage['portfolio'] = ""
    newworkpackage['portfolioOwner'] = ""
    newworkpackage['leadPerson'] = g.user.name
    newworkpackage['status'] = "Defining"
    newworkpackage['name'] = name
    newworkpackage['created'] = g.packages.rethinkTimstamp()
    
    """
    add our new work package
    """
    newid = g.packages.new_workpackage(newworkpackage)
    return redirect(url_for('workpackageviews.workpackage_detail', workpackage_id=newid))

@workpackageviews.route('/<workpackage_id>')
@workpackageviews.route('/<workpackage_id>/')
def workpackage_detail(workpackage_id):
    """
    Render workpackage detail
    """
    cols = app.config['ALLOCATION_COLUMNS_WORKPACKAGE_DETAIL_PAGE']
    start_month = app.config['ALLOCATION_START_OFFSET_WORKPACKAGE_DETAIL_PAGE']
    end_month = start_month + cols - 1
    #print 'IS_ADMIN'
    #print current_user.is_admin()
    return render_template('workpackages/panel-workpackage43.html', \
        item=g.packages.get(workpackage_id),
        portfolioList=g.packages.get_list('portfolio'),
        portfolioHistory=g.packages.history(workpackage_id),
        start_month=start_month, end_month=end_month,
        people=g.people.get_all_names(), is_admin=current_user.is_admin())

@workpackageviews.route('/<workpackage_id>/status_updates')
@workpackageviews.route('/<workpackage_id>/status_updates/')
def workpackage_status_updates(workpackage_id):
    """
    Render workpackage status updates
    """
    item = list(g.packages.get_workpackage_status_updates(workpackage_id))[0]
    return render_template('workpackages/status-updates-table.html', \
        item=item, user=current_user.name)

@workpackageviews.route('/<workpackage_id>/<action>/confirm')
@workpackageviews.route('/<workpackage_id>/<action>/confirm/')
def workpackage_confirm(workpackage_id, action):
    """
    Confirm the chosen action
    """
    if (action == "Close"):
        return render_template('workpackages/panel-close-workpackage43.html', \
            item=g.packages.get(workpackage_id))
    if (action == "Delete"):
        return render_template('workpackages/panel-delete-workpackage43.html', \
            item=g.packages.get(workpackage_id))
        
    return ""

@workpackageviews.route('/<workpackage_id>/<action>')
@workpackageviews.route('/<workpackage_id>/<action>/')
def workpackage_action(workpackage_id, action):
    """
    Carry out the chosen action
    """
    if (action == "Close"):
        # update the work package
        g.packages.update_workpackage(workpackage_id, "status", "Closed")
        g.packages.update_workpackage(workpackage_id, "closedDate", date.today().strftime("%d/%m/%Y"))
        return redirect(url_for('workpackageviews.workpackage_detail', workpackage_id=workpackage_id))
    if (action == "Delete"):
        # delete the work package
        g.packages.update_workpackage(workpackage_id, "status", "Deleted")
        g.packages.update_workpackage(workpackage_id, "closedDate", date.today().strftime("%d/%m/%Y"))
        return redirect(url_for('workpackageviews.list_workpackages_html'))
    if (action == "Duplicate"):
        """
        Make a duplicate of a record, with the allocations and id removed, and status set to "Defining"
        """
        oldworkpackage = g.packages.get(workpackage_id)
        newworkpackage = deepcopy(oldworkpackage)
        
        del newworkpackage['id']
        del newworkpackage['Assigned']
        newworkpackage['Assigned'] = []
        newworkpackage['status'] = "Defining"
        newworkpackage['name'] = newworkpackage['name'] + " (Copy)"
        
        """
        add our new duplicated work package
        """
        newid = g.packages.new_workpackage(newworkpackage)
        return redirect(url_for('workpackageviews.workpackage_detail', workpackage_id=newid))
    
    return ""

@workpackageviews.route('/<workpackage_id>/json/')
def workpackage_detail_json(workpackage_id):
    return jsonify(item=g.packages.get(workpackage_id))

@workpackageviews.route('/lists/<list_name>')
@workpackageviews.route('/lists/<list_name>/')
def get_lookup_list(list_name):
    """
    Returns distinct key/values for work packages using the given field    
    """
    
    if list_name == 'programme':
        return json.dumps(g.portfolio.get_list_values())
    else:
        return json.dumps(g.packages.get_list_values(list_name))

@workpackageviews.route('/saveitem', methods=['POST'])
@workpackageviews.route('/saveitem/', methods=['POST'])
def save_changes_from_x_editable():
    # retrieve form data
    data = request.values
    
    id = data['workpackageid']
    fieldname = data['name']
    newvalue = clean_string(data['value'])

    # retrieve work package from rethinkdb
    original = g.packages.get_workpackage(id)

    # Check if this if the percentageFteEffort field, and if so, convert it to an integer
    if fieldname == 'percentageFteEffort':
        newvalue = int(newvalue)
        
    # Check if this is the serviceType field
    if fieldname == 'serviceType':
        # Now, check if the deliverables are blank
        set_default_deliverables = False
        if 'deliverables' in original:
            if deliverables_are_empty(original['deliverables']):
                set_default_deliverables = True
        else:
            set_default_deliverables = True
        
        # Now, check if the deliverables already in this work package are still the defaults
        if set_default_deliverables == False:
            # Get the default deliverables from the previously set serviceType
            if 'serviceType' in original:
                old_default_val_entry = g.deliverables.get_values(original['serviceType'])
                old_default_val = None
                if old_default_val_entry:
                    old_default_val = old_default_val_entry['deliverables']
                
                # The deliverables were still the defaults for the old serviceType, so we can replace them
                if original['deliverables'] == old_default_val:
                    set_default_deliverables = True
        
        # Finally, if we have decided the defaults should be used, set them
        if set_default_deliverables:
            default_val = g.deliverables.get_values(newvalue)
            if default_val:
                g.packages.update_workpackage(id, 'deliverables', default_val['deliverables'])

    # update the work package
    result = g.packages.update_workpackage(id, fieldname, newvalue)
    return result

@workpackageviews.route('/addupdate', methods=['POST'])
@workpackageviews.route('/addupdate/', methods=['POST'])
def add_status_update_from_x_editable():
    # retrieve form data
    data = request.values
    
    id = data['workpackageid']
    newvalue = clean_string(data['value'])

    # retrieve work package from rethinkdb
    old_wp = g.packages.get_workpackage(id)
    updates_element = []
    if 'StatusUpdates' in old_wp:
        updates_element = old_wp['StatusUpdates']

    # Check if this if the percentageFteEffort field, and if so, convert it to an integer
    status_update = {}
    status_update['update'] = newvalue
    # Using the new rethinkdb datetime support
    status_update['date'] = g.packages.rethinkTimstamp()
    #status_update['date'] = date.today()
    status_update['author'] = current_user.name
    status_update['report_tags'] = []
    
    updates_element.append(status_update);

    #print updates_element

    # update the work package
    result = g.packages.update_workpackage_noaudit(id, 'StatusUpdates', updates_element)
    return "Done"

@workpackageviews.route('/save', methods=['POST'])
@workpackageviews.route('/save/', methods=['POST'])
def save_changes_from_aloha_editor():
    # retrieve form data
    data = request.values
    id = data['workpackageid']
    fieldname = data['contentId']
    #newvalue = unidecode(data['content'])
    newvalue = clean_string(data['content'])

    result = g.packages.update_workpackage(id, fieldname, newvalue)
    
    return result


@workpackageviews.route('/wordcount/<field>')
def wordcount(field):
    words_to_ignore = ["that","what","with","this","would","from","your","which","while","these"]
    things_to_strip = [".",",","?",")","(","\"",":",";","'s"]
    words_min_size = 4
    print_in_html = True

    text = ''.join(g.packages.get_words_for_analysis(field))
    s = MLStripper()
    s.feed(text)
    text2 = s.get_data()

    words = text.lower().split()
    #print words[0]

    wordcount = {}
    for word in words:
        for thing in things_to_strip:
            if thing in word:
                word = word.replace(thing,"")
        if word not in words_to_ignore and len(word) >= words_min_size:
            if word in wordcount:
                wordcount[word] += 1
            else:
                wordcount[word] = 1
        
    sortedbyfrequency =  sorted(wordcount,key=wordcount.get,reverse=True)
     
    stra = ""
    for word in sortedbyfrequency:
        stra = stra + word + "," + str(wordcount[word]) + "\n" 
    headers = {}
    headers['Content-Disposition']='filename="'+field+'.csv"'
    return Response(stra, mimetype='text/csv', headers=headers)

"""
Strip out any html tags, then trim the result to see if we have any text
"""
def deliverables_are_empty(value):
    tags_removed = ""
    in_tag = False
    for c in value:
        if in_tag:
            if c == '>':
                in_tag = False
        else:
            if c == '<':
                in_tag = True
            else:
                tags_removed = tags_removed + c
    
    if len(tags_removed.strip()) == 0:
        return True
    else:
        return False

class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)
    
