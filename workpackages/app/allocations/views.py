"""
View logic for allocations
"""

from flask import Blueprint, render_template, request, g, jsonify, \
                redirect, url_for, session
from flask_login import current_user
from app.datastores.packages import PackagesStore
from app.utils import *
from app import app, lm
#from app import app
from datetime import date
import json, copy
from app.datastores.packagefilters import PackageFilters

allocationviews = Blueprint('allocationviews', __name__,
                        template_folder='templates')

@allocationviews.before_request
def before_request():
    if not current_user.is_authenticated:
        return lm.unauthorized()
    
    g.packages = PackagesStore(g.rdb_conn)

@allocationviews.route('/saveallocation', methods=['POST', 'GET'])
@allocationviews.route('/saveallocation/', methods=['POST', 'GET'])
def save_allocation_changes_from_x_editable():
    # retrieve form data
    data = request.values
    
    id = data['workpackageid']
    fieldname = data['month']
    user = data['user']
    newvalue = int(data['value'])

    # retrieve work package from rethinkdb
    original = g.packages.get_workpackage(id)
    
    # update the work package
    update = original

    userFound = False
    
    for field in update['Assigned']:
        #print user
        #print field['Name']
        #print '----'
        if field['Name']==user:
            field['Allocations'][fieldname] = newvalue
            #print 'user found ' + user
            userFound = True
            break

    #print update
    if not userFound:
        #print 'user NOT found ' + user
        update['Assigned'].append({'Name':user,'Status':'Active', 'Allocations':{ }})

    #print update
    g.packages.update_allocation(id, update)
    return "Updated"


@allocationviews.route('/save_person_status', methods=['POST'])
@allocationviews.route('/save_person_status/', methods=['POST'])
def save_person_status():
    # retrieve form data
    data = request.values
    
    id = data['workpackageid']
    user = data['user']
    newvalue = data['value']

    # retrieve work package from rethinkdb
    original = g.packages.get_workpackage(id)
    
    # update the work package
    update = original

    userFound = False
    
    for field in update['Assigned']:
        if field['Name']==user:
            field['Status'] = newvalue
            userFound = True
            break

    if userFound:
        g.packages.update_allocation(id, update)
    
    return "Updated"


@allocationviews.route('/remove', methods=['POST'])
@allocationviews.route('/remove/', methods=['POST'])
def remove_allocated_person():
    # retrieve form data
    data = request.values
    wp_id = data['workpackageid']
    user = data['user']
    # Check if this user has any time allocated already
    result = g.packages.get_total_allocation_for_person(wp_id, user)
    total = int(result[0]['total_allocation_for_person'])
    #print total

    if total > 0:
        return "This person has allocations already and cannot be removed."
    else:
        # retrieve work package from rethinkdb
        original = g.packages.get_workpackage(wp_id)
        # update the work package
        update = original
        userFound = False
        for field in update['Assigned']:
            if field['Name']==user:
                update['Assigned'].remove(field)
                userFound = True
                break
        if userFound:
            g.packages.update_allocation(wp_id, update)
            #print "New WP:"
            #print update
        else:
            return "User was not assigned to this work package."
    return "Done"

@allocationviews.route('/')
def get_allocations():
    
    admin = "False"
    if current_user.is_admin():
        admin = "True"
    
    cols = app.config['ALLOCATION_COLUMNS_PERSON_PAGE']
    start_month = app.config['ALLOCATION_START_OFFSET_PERSON_PAGE']
    end_month = start_month + cols - 1
    return render_template('allocations/panel-status43.html',
                    start_month=start_month, end_month=end_month,
                    name=g.user.name,
                    portfolio=g.user.programme,
                    portfolio_lead=g.user.name,
		            lead=g.user.lead,
                    admin=admin,
                    directorate_lead='Shaun Fletcher')


@allocationviews.route('/view')
@allocationviews.route('/view/')
def get_all_allocations_html():
    data = get_all_allocations()
    template_name = 'allocations/allocations-table.html'
    #print data
    return render_template(template_name,
                            type=data['type'],
                            assignments=data['assignments'],
                            month_names_for_display=data['month_names_for_display'],
                            month_names=data['month_names'],
                            start_month=data['start_month'],
                            end_month=data['end_month'],
                            month_count=data['month_count'],
                            workpackage_id=data['workpackage_id'],
                            person=data['person'],
                            status=data['status'],
                            table_id=data['table_id'])
        
@allocationviews.route('/view/json')
@allocationviews.route('/view/json/')
def get_all_allocations_json():
    data = get_all_allocations()
    return jsonify(data)

def get_all_allocations():
    """ 
    Get a list of all allocations matching the provided criteria
    Note: To get summarised allocations use the get_summary_allocations instead 
    Request criteria:
    - start_month: offset for the month to start from (inclusive) 
                   0 = Current Month, 1 = Next Month, -1 = Last Month, etc)
    - end_month: offset for the month to end at (inclusive)
    - person: Person whose allocations we want to see
    - workpackage_id: Specific work package we want to see allocations for
    """
    filtered_args = filter_request_args(request.args, FIELD_LIST['allocations'])

    # Get the values from the request
    start_month = int(filtered_args['start_month'])
    end_month = int(filtered_args['end_month'])
    
    if 'table_id' in filtered_args:
        table_id = filtered_args['table_id']
    else:
        table_id = 'tblAllocations'
    
    status = None
    if 'status' in request.args:
        status = filtered_args['status']
    
    #TODO: Error handling when parameters are missing
    month_count = end_month - start_month + 1
    month_names = []
    month_names_for_display = []
    
    # Populate an array with the month names we want
    for x in range(start_month, end_month+1):
        month_names.append(month_name(x))
        month_names_for_display.append(month_name_display(x))

    data = {}
    data['month_names_for_display']=month_names_for_display
    data['month_names']=month_names
    data['start_month']=start_month
    data['end_month']=end_month
    data['month_count']=month_count
    data['person']=''
    data['workpackage_id']=''
    data['status']=status
    data['table_id']=table_id
    
    if 'person' in request.args:
        person = filtered_args['person']
        assignments = g.packages.get_allocations_person(person, month_names, status)
        
        """
        Remove entries where the person's allocation has ended and there is no work
        in any of the visible months
        """
        new_assignments = assignments
        new_assignments = []
        for assignment in assignments:
            include = False
            if assignment['allocations'][0]['personStatus'] == "Active":
                include = True
            for allocation_val in assignment['allocations'][0]['months']:
                if int(allocation_val) > 0:
                    include = True
            
            """
            Add any necessary warning indicators - e.g. end date has been passed
            """
            assignment['warning'] = None
            if assignment['forecastEndDate'] is None:
                assignment['warning'] = 'No estimated end date specified'
            else:
                end_date = parse_date(assignment['forecastEndDate'])
                now = datetime.now()
                if end_date is None:
                    assignment['warning'] = 'No estimated end date specified'
                elif now > end_date:
                    assignment['warning'] = 'Estimated end date has been passed'
            
            if include:
                new_assignments.append(assignment)
        
        data['type'] = 'person'
        data['assignments'] = new_assignments
        data['person']=person
        #print data
        return data
        
    if 'workpackage_id' in request.args:
        workpackage_id = filtered_args['workpackage_id']
        assignments = g.packages.get_allocations_wp(workpackage_id, month_names)
        
        data['type'] = 'workpackage'
        data['assignments'] = assignments
        data['workpackage_id'] = workpackage_id
        return data
    
    return ""
