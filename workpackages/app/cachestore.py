from werkzeug.contrib.cache import SimpleCache
cache = SimpleCache()

class CacheStore:
    """
    Mega simple caching for datastore
    
    For more information, see http://flask.pocoo.org/docs/patterns/caching/ 
    """
    def __init__(self, key, minutes=10, vary_by=None):
        self.key = key
        self.minutes = minutes
        self.vary_by = vary_by

    def __call__(self, f):
    
        def check_cache(*args, **kwargs):
            cache_key = "store_" + self.key
            if self.vary_by:
                varies_by = '~'.join([args[vary].__str__() for vary in self.vary_by])
                cache_key = "%s_%s" %  (cache_key, varies_by)
            
            cached = cache.get(cache_key)
            if cached is None:
                #print "CACHE MISS: " + cache_key
                cached = f(*args, **kwargs)
                cache.set(cache_key, cached, timeout=self.minutes * 60)
            return cached
        
        return check_cache
