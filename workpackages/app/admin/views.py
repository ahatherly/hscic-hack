"""
View logic for administrative tasks
"""
from flask import Blueprint, render_template, request, g, url_for, redirect
from flask_login import current_user
from flask_api import status
from app.models import User
from app.datastores.people import PeopleStore
from app.datastores.approvals import ApprovalsStore
from app.datastores.reference import ReferenceStore
from app.datastores.deliverables import DeliverablesStore

from app.utils import *
from app import app, lm

adminviews = Blueprint('adminviews', __name__,
                        template_folder='templates')

@adminviews.before_request
def before_request():
    if not current_user.is_authenticated:
        return lm.unauthorized()
    if not current_user.is_admin():
        return lm.unauthorized()

    g.people = PeopleStore(g.rdb_conn)
    g.approvals = ApprovalsStore(g.rdb_conn)
    g.reference = ReferenceStore(g.rdb_conn)
    g.deliverables = DeliverablesStore(g.rdb_conn)

@adminviews.route('/')
def admin_index():
    """
    Admin handler
    """
    return render_template('admin/index43.html')

@adminviews.route('/approvals')
def admin_approvals():
    return render_template('admin/approvals43.html',
        unapproved=g.approvals.get_unapproved())

@adminviews.route('/userlist')
def user_list():
    return render_template('admin/userlist43.html',userlist=list(g.people.get_all()))

@adminviews.route('/approvals/count')
def admin_approvals_count():
    return str(g.approvals.get_unapproved_count())

@adminviews.route('/approvals/confirm/<approval_id>')
def confirm_user(approval_id):  
    to_approve = g.approvals.get(approval_id)
    if to_approve is None:
        abort('the user is no longer in the list of approvals')

    del to_approve["id"]
    to_approve['status'] = "Active"
    
    """
    If an entry already exists for this name, overwrite it
    """
    old_user = g.people.one_by_name(to_approve['name'])
    if old_user is not None:
        id = old_user.id
        to_approve['id'] = id

    g.people.post(to_approve)
    g.approvals.delete(approval_id)
    return redirect(url_for('adminviews.admin_approvals'))

@adminviews.route('/approvals/reject/<approval_id>')
def reject_user(approval_id):
    g.approvals.delete(approval_id)
    return redirect(url_for('adminviews.admin_approvals'))

@adminviews.route('/saveitem', methods=['POST'])
@adminviews.route('/saveitem/', methods=['POST'])
def save_changes_from_x_editable():
    # retrieve form data
    data = request.values
    
    id = data['personid']
    fieldname = data['fieldname']
    newvalue = clean_string(data['value'])

    # retrieve user package from rethinkdb
    original = g.people.get(id)

    # update the user
    result = g.people.update_person(id, fieldname, newvalue)
    return result

@adminviews.route('/delete')
@adminviews.route('/delete/')
def remove_person():
    person_id = request.values['person_id']
    g.people.delete_person(person_id)
    return redirect(url_for('adminviews.user_list'))

@adminviews.route('/reference')
@adminviews.route('/reference/')
def reference_lists():
    data = list(g.reference.get_all())
    return render_template('admin/reference43.html',data=data)

@adminviews.route('/deliverables')
@adminviews.route('/deliverables/')
def deliverables():
    data = list(g.deliverables.get_all())
    return render_template('admin/deliverables43.html',data=data)

@adminviews.route('/savereferenceitem', methods=['POST'])
@adminviews.route('/savereferenceitem/', methods=['POST'])
def save_reference_data_changes_from_x_editable():
    
    # retrieve form data
    data = request.values
    
    field_name = data['field_name']
    index = int(data['index'])
    newvalue = clean_string(data['value'])

    reference_values = g.reference.get_values(field_name)
    oldvalue = reference_values['field_values'][index-1]
    
    #print "Request to update field: " + field_name + " old value: " + oldvalue + " new value: " + newvalue
    
    # Make the change to the new value
    reference_values['field_values'][index-1] = newvalue
    # Save it
    result = g.reference.post(reference_values)
    return ""

@adminviews.route('/savedeliverableitem', methods=['POST'])
@adminviews.route('/savedeliverableitem/', methods=['POST'])
def save_deliberable_data_changes_from_x_editable():
    
    # retrieve form data
    data = request.values
    
    field_name = data['field_name']
    id = data['id']
    newvalue = clean_string(data['value'])

    deliverable_value = g.deliverables.get(id)
    
    #print "Request to update field: " + field_name + " new value: " + newvalue
    
    # Make the change to the new value
    deliverable_value[field_name] = newvalue
    # Save it
    result = g.deliverables.post(deliverable_value)
    return ""

@adminviews.route('/savedeliverables', methods=['POST'])
@adminviews.route('/savedeliverables/', methods=['POST'])
def save_deliverable_aloha_field():
    
    # retrieve form data
    data = request.values
    #fieldId = data['fieldId']
    fieldId = data['contentId']
    newvalue = clean_string(data['content'])

    result = g.deliverables.update_field(fieldId, 'deliverables', newvalue)
    
    #return result
    return ""


@adminviews.route('/newreferenceitem', methods=['POST'])
@adminviews.route('/newreferenceitem/', methods=['POST'])
def new_reference_data_entry_from_x_editable():
    
    # retrieve form data
    data = request.values
    
    field_name = data['field_name']
    newvalue = clean_string(data['value'])

    reference_values = g.reference.get_values(field_name)
    
    #print "Request to update field: " + field_name + " to add new value: " + newvalue
    
    # Check the value isn't already in there
    if newvalue in reference_values['field_values']:
        return ""
    
    # Add the new value
    reference_values['field_values'].append(newvalue)
    # Save it
    result = g.reference.post(reference_values)
    return ""

@adminviews.route('/newdeliverable', methods=['POST'])
@adminviews.route('/newdeliverable/', methods=['POST'])
def new_deliverable_entry_from_x_editable():
    
    # retrieve form data
    data = request.values
    serviceType = clean_string(data['value'])

    existingEntry = g.deliverables.get_values(serviceType)
    
    if (existingEntry):
        content = 'That service type already exists!'
        return content, status.HTTP_400_BAD_REQUEST
    
    #print "Request to add deliverable entry: " + serviceType
    
    item = {}
    item['serviceType'] = serviceType
    item['deliverables'] = 'Enter deliverables here'
    # Save it
    result = g.deliverables.post(item)
    return ""


@adminviews.route('/deletereferenceitem')
@adminviews.route('/deletereferenceitem/')
def delete_reference_data_item():
    
    # retrieve form data
    data = request.values
    
    field_name = data['field_name']
    index = int(data['index'])

    reference_values = g.reference.get_values(field_name)
    oldvalue = reference_values['field_values'][index-1]
    
    #print "Request to update field: " + field_name + " to remove value: " + oldvalue
    
    # Make the change to the new value
    reference_values['field_values'].pop(index-1)
    # Save it
    result = g.reference.post(reference_values)
    
    return redirect(url_for('adminviews.reference_lists'))

@adminviews.route('/deletedeliverables')
@adminviews.route('/deletedeliverables/')
def delete_deliverables_item():
    
    # retrieve form data
    data = request.values
    field_id = data['id']

    # Delete it
    result = g.deliverables.delete(field_id)
    
    return redirect(url_for('adminviews.deliverables'))
