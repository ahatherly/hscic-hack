from flask import abort, render_template, flash, redirect, \
    session, url_for, request, g, make_response

from flask_login import login_user, logout_user, current_user, login_required
from flask_wtf import Form

from wtforms import TextField, BooleanField
from wtforms.validators import Required

#from app import app, authomatic, lm
from app import app, oidc, lm
from forms import LoginForm, EditForm, NewUserForm
from models import User 
from app.datastores.approvals import ApprovalsStore
from app.datastores.people import PeopleStore

from authomatic.adapters import WerkzeugAdapter
from authomatic import Authomatic

#from config import CONFIG, TEST_TOKEN

import rethinkdb as r
import json
from rethinkdb.errors import RqlRuntimeError, RqlDriverError
from utils import *

@app.teardown_request
def teardown_request(exception):
    try:
        g.rdb_conn.close()
    except AttributeError:
        pass

@app.before_first_request
@app.before_request
def before_request():
    try:
        g.rdb_conn = r.connect(host=app.config['RDB_HOST'], 
            port=app.config['RDB_PORT'], 
            db=app.config['DB_NAME'])
    except RqlDriverError:
        abort(503, "No database connection could be established.")
    g.user = current_user
    g.approvals = ApprovalsStore(g.rdb_conn)
    g.people = PeopleStore(g.rdb_conn)

@app.after_request
def add_header(response):
    """
    Add no-cache header
    """
    response.headers['Cache-Control'] = 'no-cache, no-store, must-revalidate'
    response.headers['Pragma'] = 'no-cache'
    response.headers['Expires'] = '0'
    return response

@app.route('/')
def index():
    """
    Home handler
    """
    if current_user.is_authenticated:
        return redirect(url_for('allocationviews.get_allocations'))

    return redirect(url_for('login'))

"""
@app.route('/login/<provider_name>/', methods=['GET', 'POST'])
def login(provider_name):
    Login handler, must accept both GET and POST to be able to use OpenID.
    
    # This method will only work on test servers - using token in secret.py
    if (provider_name == 'test'):
        if TEST_TOKEN == None:
            return redirect(url_for('index'))
        if len(TEST_TOKEN) < 5:
            return redirect(url_for('index'))
        else:
            filtered_args = filter_request_args(request.args, FIELD_LIST['login'])
            if 'token' in request.args and 'username' in request.args:
                token = filtered_args['token']
                username = filtered_args['username']
                if token == TEST_TOKEN:
                    mock = g.people.one_by_name(username)
                    login_user(mock, remember = True)
                    return redirect(url_for('allocationviews.get_allocations'))
                else:
                    return redirect(url_for('index'))
            else:
                return redirect(url_for('index'))

    response = make_response()
    result = authomatic.login(WerkzeugAdapter(request, response), provider_name)

    if result:
        if result.user:
            #print "provider_name"
            #print provider_name
            #print "result.user"
            #print dump(result.user)
            #print "result"
            #print dump(result)
            
            result.user.update()
            #print "result.user (after .update() )"
            #print dump(result.user)
            
            existing_user = None
            if result.user.id != None and result.user.id != "None" and len(result.user.id) > 4:
                existing_user = g.people \
                    .get_user_by_auth(provider_name, result.user.id)
	    else:
		return redirect(url_for('index'))

            if existing_user and existing_user.is_active():
                g.people.user_logged_in(existing_user.id)
                login_user(existing_user, remember = True)
                return redirect(url_for('allocationviews.get_allocations'))

            elif existing_user:
                #return redirect(url_for('inactiveuser'))
                return redirect(url_for('allocationviews.get_allocations'))

            else:
                session['auth_id'] = result.user.id
                session['auth_username'] = result.user.username
                session['auth_name'] = result.user.name
                session['auth_provider_name'] = provider_name
                session['auth_email'] = result.user.email

                return redirect(url_for('register'))

        return render_template('login.html', result=result)

    return response

def dump(obj):
  for attr in dir(obj):
    print "obj.%s = %s" % (attr, getattr(obj, attr))
"""

@app.route('/login')
@oidc.require_login
def login():
    #return 'Welcome %s, your auth token contains:' % oidc.user_getfield('email')
    #return redirect("/", code=302)
    #print g.oidc_id_token
    email = oidc.user_getfield('email')
    existing_user = get_user_by_email(email)
    
    if existing_user and existing_user.is_active():
        g.people.user_logged_in(existing_user.id)
        login_user(existing_user, remember = True)
        return redirect(url_for('allocationviews.get_allocations'))
    else:
        session['auth_id'] = oidc.user_getfield('sub')
        session['auth_username'] = oidc.user_getfield('email')
        session['auth_name'] = oidc.user_getfield('name')
        session['auth_provider_name'] = 'keycloak'
        session['auth_email'] = oidc.user_getfield('email')
        return redirect(url_for('register'))


@app.route('/register', methods = ['GET', 'POST'])
def register():
    form = NewUserForm()
    if form.validate_on_submit():

        user=User(form.name.data,
                  authid=session['auth_id'], \
                  authprovider=session['auth_provider_name'], \
                  email=form.email.data, \
                  emailauth=session['auth_email'], \
                  jobtitle=form.jobtitle.data)
        g.approvals.add_pending(user)
        return redirect('/pendingapproval')

    return render_template('newuser43.html', 
        title = 'Register new user',
        form = form)
"""

"""
@app.route('/pendingapproval')
def pendingapproval():
    return render_template('pendingapproval.html')
"""

"""
@lm.unauthorized_handler
@app.route('/inactiveuser')
def inactiveuser():
    return render_template('inactiveuser.html')

@lm.user_loader
def load_user(user_id):

    # TODO: refactor, check for existing connection
    g.rdb_conn = r.connect(host=app.config['RDB_HOST'], 
            port=app.config['RDB_PORT'], 
            db=app.config['DB_NAME'])
    people = PeopleStore(g.rdb_conn)

    # TODO: ?redirect for not having work email / approval ...
    return people.get_session_user(user_id)


def get_user_by_email(e):
    cursor = r.table('people') \
        .filter(dict(email=e)) \
        .run(g.rdb_conn)

    users = list(cursor)
    if (len(users) == 1):
        user = users[0]
        return User.map_to(user)
    else:
        return None
    #return map_db_user_to_model(user)

@app.route('/logout')
def logout():
    oidc.logout()
    logout_user()
    print request
    host = request.url_root
    #return render_template('loggedout.html', logout_uri=oidc.client_secrets['logout_uri'])
    url = oidc.client_secrets['logout_uri'] + "?redirect_uri=" + host + url_for("login")
    return redirect(url)

@app.route('/account')
def account():
    url = oidc.client_secrets['account_uri']
    return redirect(url)

@app.route('/loggedout')
def logged_out():
    return render_template('loggedout.html')

@app.route('/robots.txt')
def static_from_root():
    return send_from_directory(app.static_folder, request.path[1:])