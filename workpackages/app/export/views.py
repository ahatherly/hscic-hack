"""
Generates exports - including a word version of a work package
"""
from flask import Blueprint, render_template, redirect, url_for, send_file, g
from flask_login import current_user
from app.datastores.packages import PackagesStore
from app.datastores.packagefilters import PackageFilters
from app import app, lm
import subprocess

exportviews = Blueprint('exportviews', __name__,
                        template_folder='templates')

@exportviews.before_request
def before_request():
    if not current_user.is_authenticated:
        return lm.unauthorized()
    g.packages = PackagesStore(g.rdb_conn)

@exportviews.route('/<workpackage_id>/word')
@exportviews.route('/<workpackage_id>/word/')
def export_post_word(workpackage_id):
    # show the work package with the given id as a word document using pandoc
    generatedfilespath=app.config['GENERATED_FILES_PATH']
    referencedocpath=app.config['REFERENCE_DOC_PATH']
    
    """
    Get the item, but also add estimated effort, etc to it (calculated fields)
    """
    #item = g.packages.get(workpackage_id)
    results = PackageFilters(g.packages).filter({'id':workpackage_id}) \
                            .execute().add_expected_effort_and_date_checks().val()
    #print results;
    item = results[0]
    
    htmlversion = render_template('export/wordTemplate.html', item=item)
    filename=generatedfilespath+workpackage_id+'.docx'
    args = ['pandoc', '--reference-docx=' + referencedocpath, '-o', filename, '--from=html']
    #args = ['pandoc', '-o', filename, '--from=html']
    #print args
    p = subprocess.Popen(
                args,
                stdin=subprocess.PIPE, 
                stdout=subprocess.PIPE
    )
    p.communicate(htmlversion)[0]
    
    pretendfilename=item['name']+'.docx'
    
    # Note! - when referring to the application's static folder from within a submodule, you have to add a 
    # leading . as per the note at the bottom of: https://flask.readthedocs.org/en/0.6/patterns/packages/
    #return redirect(url_for('.static', filename='temp/'+workpackage_id+'.docx'))
    
    return send_file(filename, mimetype='application/vnd.openxmlformats-officedocument.wordprocessingml.document', \
                     as_attachment=True, attachment_filename=pretendfilename)
