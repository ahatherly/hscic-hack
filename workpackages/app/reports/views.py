"""
Report views
"""
from flask import Blueprint, render_template, request, redirect, url_for, g, jsonify, Response
from flask_login import current_user
from app.datastores.packages import PackagesStore
from app.datastores.people import PeopleStore
from app.datastores.portfolio import PortfolioStore
from app.utils import *
from app import app, lm
import json, copy

reportviews = Blueprint('reportviews', __name__,
                        template_folder='templates')

@reportviews.before_request
def before_request():
    if not current_user.is_authenticated:
        return lm.unauthorized()
    g.packages = PackagesStore(g.rdb_conn)
    g.people = PeopleStore(g.rdb_conn)
    g.programmes = PortfolioStore(g.rdb_conn)

@reportviews.route('/')
def show_reports():
    """ 
    Show the reports pages
    """
    return render_template('reports/panel-reports43.html')

@reportviews.route('/pivot')
@reportviews.route('/pivot/')
def show_pivot():
    """ 
    Data for the pivot table
    """
    filtered_args = filter_request_args(request.args, FIELD_LIST['allocations'])

    # Get the values from the request
    if 'start_month' in request.args:
        start_month = int(filtered_args['start_month'])
    else:
        start_month = -2
    
    if 'end_month' in request.args:
        end_month = int(filtered_args['end_month'])
    else:
        end_month = 4
    
    #TODO: Error handling when parameters are missing
    month_count = end_month - start_month + 1
    month_names = []
    month_as_date = []
    
    # Populate an array with the month names we want
    for x in range(start_month, end_month+1):
        month_names.append(month_name(x))
        month_as_date.append(month_as_full_date(x))
    
    wplist = g.packages.get_allocations_all(month_names)


    # Reformat for pivot.js
    pivot_fields = ["name","status","programme","areaOfWork", "forecastStartDate", "forecastEndDate", "leadPerson"]
    pivot_field_display_names = ["WorkpackageName","WorkpackageStatus","Programme","Domain",
                                 "ForecastStartDate", "ForecastEndDate", "LeadPerson"]
    data = [];
    index = 0
    for item in wplist:
        if (index > -1):
            pivotItem = {};
            fieldIndex = 0
            for field in pivot_fields:
                name = pivot_field_display_names[fieldIndex]
                if field in item['wp']:
                    val = item['wp'][field]
                    pivotItem[name] = val
                else:
                    pivotItem[name] = ''
                fieldIndex += 1
                    
            # Now replicate the row for each allocation
            allocations = item['allocations']
            if len(allocations) == 0:
                pass
                #pivotItem['allocatedPerson'] = "Unallocated"
                #pivotItem['allocatedEffort'] = 0
                #data.append(pivotItem)
            else:
                # Loop through the people allocated
                for allocationEntry in allocations:
                    # Now loop through the allocations for the next few months for that person
                    month_index = start_month

                    for allocated_mandays in allocationEntry['months']:
                        monthname = month_name(month_index)
                        daysinmonth = business_days_in_month(monthname)
                        if (allocated_mandays) > 0:
                            pivotItemCopy = copy.copy(pivotItem)
                            pivotItemCopy['AllocatedPerson'] = allocationEntry['Name']
                            pivotItemCopy['AllocationMonth'] = month_and_year_name_year_first(month_index)
                            pivotItemCopy['AllocationEffort'] = allocated_mandays
                            pivotItemCopy['DaysInMonth'] = daysinmonth
                            
                            percentage = (float(allocated_mandays) / float(daysinmonth)) * 100
                            pivotItemCopy['AllocationEffortAsPercentage'] = percentage
                            data.append(pivotItemCopy)
                        month_index += 1
        index += 1
        
    """
    Pivot.js needs the data in a flat list, with the first row being the headings
    
    wp_headings = ["name","status","portfolio","programme","portfolioOwner","areaOfWork","serviceType","level1Type", "level2Type", "forecastStartDate", "forecastEndDate", "percentageFteEffort", "leadPerson", "workpackageCount"]
    allocation_headings = ["allocatedPerson", "month", "mandaysAllocated"]
    all_headings = wp_headings + allocation_headings
    
    data = []
    data.append(all_headings)
    
    for item in wplist:
        row = []
        wp_fields = item['wp']
        for col in wp_headings:
            if col in wp_fields:
                row.append(wp_fields[col])
            else:
                if (col == 'workpackageCount'):
                    row.append('1')
                else:
                    row.append('')
        
        
        #Now, add a copy of the row for each allocation, with the relevant values in
        
        allocations = item['allocations']
        dummy_assignment = ["", "", 0]

        if len(allocations) == 0:
            row = row + dummy_assignment
            data.append(row)
        
        for allocation in allocations:
            person_name = allocation['Name']
            person_status = allocation['Status']
            month_index=0
            for allocated_mandays in allocation['months']:
                new_row = []
                new_row = new_row + row
                new_row.append(person_name)
                new_row.append(month_as_date[month_index])
                if allocated_mandays == "":
                    new_row.append(0)
                else:
                    new_row.append(allocated_mandays)
                data.append(new_row)
                month_index = month_index + 1
                
    return json.dumps(data)
    """
    return json.dumps(data)

@reportviews.route('/allocation_table')
@reportviews.route('/allocation_table/')
def show_allocation_table():
    filtered_args = filter_request_args(request.args, FIELD_LIST['allocations'])
    # Get the values from the request
    start_month = int(filtered_args['start_month'])
    end_month = int(filtered_args['end_month'])
    #TODO: Error handling when parameters are missing
    month_count = end_month - start_month + 1
    month_names = []
    month_names_for_display = []
    
    # Populate an array with the month names we want
    for x in range(start_month, end_month+1):
        month_names.append(month_name(x))
        month_names_for_display.append(month_name_display(x))

    data = {}
    data['month_names_for_display']=month_names_for_display
    data['month_names']=month_names
    data['start_month']=start_month
    data['end_month']=end_month
    data['month_count']=month_count
    data['people'] = []
    
    person_list = g.people.get_all_names()
    
    for person_name in person_list:
        person_entry = {}
        person_entry['name'] = person_name
        person_entry['value'] = []
        person_month_totals = []
        for x in month_names:
            person_month_totals.append(0)
        
        assignments = g.packages.get_allocations_person(person_name, month_names, 'Active')
        for assignment in assignments:
            # Use a list comprehension to add the elements in the lists
            # http://stackoverflow.com/questions/14050824/add-sum-of-values-of-two-lists-into-new-list
            person_month_totals = \
                [x + y for x, y in zip(person_month_totals, assignment['allocations'][0]['months'])]
        
        # Convert to percentage
        for month_index in range(0, month_count):
            working_days = business_days_in_month(month_names[month_index])
            percent = int((float(person_month_totals[month_index]) / float(working_days)) * 100)
            value = {}
            value['percent'] = percent
            value['mandays'] = person_month_totals[month_index]
            if percent >=100:
                value['colour_index'] = '30'
            else:
                value['colour_index'] = str(int(percent/4))
            person_entry['value'].append(value)
        
        data['people'].append(person_entry)   
    
    return render_template('reports/allocationColourTable.html', data=data)


@reportviews.route('/graph')
@reportviews.route('/graph/')
def show_graph():
    filtered_args = filter_request_args(request.args, FIELD_LIST['graphs'])

    # Get the values from the request
    if 'month' in request.args:
        month_offset = int(filtered_args['month'])
    else:
        month_offset = 0
        
    month = month_name(month_offset)
    month_display = month_name_display(month_offset)
    
    #treeMapSums = g.packages.get_total_allocation_for_month_by_portfolio_and_domain(month)
    #treeMapCounts = g.packages.get_count_for_month_by_portfolio_and_domain(month)
    fullList = g.packages.get_total_allocations_for_month(month)
    
    """
    Build the data structure needed for the treemap
    """

    """
     [ {u'reduction': 3, u'group': [u'Local Service, Child Health, Public Health', u'ITK']},
       {u'reduction': 36, u'group': [u'Local Service, Child Health, Public Health', u'LPfIT']} ]
    """
    
    # Header row
    # ['Location', 'Parent', 'Market trade volume (size)', 'Market increase/decrease (color)'],
    treeMapData = []
    treeMapData.append(['Item', 'Parent', 'Mandays Allocated (size)', 'Number of Work Packages (colour)', 'Tooltip'])
    treeMapData.append(['Mandays Allocated', None, 0, 0, ""])
    
    # Data rows
    # ['America',   'Global',             0,                               0],
    portfolioList = []
    domainList = []
    cleanedDomains = {}
    uniqueEntries = []
    for item in fullList:
        serviceType = "None"
        domain = "Unknown"
        allocated = item['total_allocation_for_month']
        if 'areaOfWork' in item:
            if not item['areaOfWork'] == None:
                if not item['areaOfWork'] == "":
                    domain = item['areaOfWork'] 
        if 'serviceType' in item:
            if not item['serviceType'] == None:
                if not item['serviceType'] == "":
                    serviceType = item['serviceType']
        
        name = item['name']
        
        # First entry for this domain
        if not domain in portfolioList:
            # Add the domain to the treemap
            headerRow = []
            headerRow.append(domain)
            headerRow.append('Mandays Allocated')
            headerRow.append(0)
            headerRow.append(0)
            headerRow.append('')
            treeMapData.append(headerRow)
            portfolioList.append(domain)
        
        # Clean-up and de-dup the serviceType names
        key = domain + "-" + serviceType
        if not key in cleanedDomains.keys():
            keyEntry = {}
            keyEntry['domain'] = domain
            # The serviceType needs to be unique too
            while serviceType in domainList:
                serviceType = serviceType + " "
            keyEntry['serviceType'] = serviceType
            cleanedDomains[key] = keyEntry
            
            # Add the serviceType to the treemap
            headerRow = []
            headerRow.append(serviceType)
            headerRow.append(domain)
            headerRow.append(0)
            headerRow.append(0)
            headerRow.append('')
            treeMapData.append(headerRow)
            domainList.append(serviceType)
        
        else:
            keyEntry = cleanedDomains[key]
            domain = keyEntry['domain']
            serviceType = keyEntry['serviceType']
        
        # Make sure the WP name is unique (by adding trailing spaces as required
        while name in uniqueEntries:
            name = name + " "
        uniqueEntries.append(name)
        
        tooltipText = "<b>Status:</b> " + get_value_for_display(item,'status') + \
                        "<br><b>Percentage FTE Effort Expected:</b> " + get_value_for_display(item,'percentageFteEffort') + "%" + \
                        "<br><b>Lead person:</b> " + get_value_for_display(item,'leadPerson') + \
                        "<br><b>Start Date:</b> " + get_value_for_display(item,'forecastStartDate') + \
                        "<br><b>End Date:</b> " + get_value_for_display(item,'forecastEndDate')
        
        # Extract the specific allocations
        isFirstAllocation = True
        if 'Assigned' in item.keys():
            for allocationItem in item['Assigned']:
                personName = get_value_for_display(allocationItem,'Name')
                mandays = 0
                if 'Allocations' in allocationItem: 
                    if month in allocationItem['Allocations']:
                        mandays = int(allocationItem['Allocations'][month])
                if mandays > 0:
                    if isFirstAllocation:
                        tooltipText = tooltipText + "<br><b>Specific Allocations:</b>"
                        isFirstAllocation = False
                    tooltipText = tooltipText + "<br><li>" + personName + ": " + str(mandays) + " days" 
        
        # Add the data row
        row = []
        row.append(name)
        row.append(serviceType)
        row.append(allocated)
        row.append(1)
        row.append(tooltipText)
        treeMapData.append(row)

    return render_template('reports/graphs.html', \
                           treeMap=json.dumps(treeMapData), \
                           month_offset=month_offset, \
                           month_display=month_display)


@reportviews.route('/dq')
@reportviews.route('/dq/')
def show_dq():
    month = month_name(0)
    result = g.packages.get_dq_summary(month)
    return render_template('reports/portfolio-dq.html', result=result, this_month = month_name(0), last_month = month_name(-1))

@reportviews.route('/directorate')
@reportviews.route('/directorate/')
def show_directorate_summary():
    filtered_args = filter_request_args(request.args, FIELD_LIST['reports'])
    if 'group_field' in request.args:
        group_field = filtered_args['group_field']
    else:
        group_field = 'areaOfWork'
    
    group_field
    result = g.packages.get_portfolio_summary(group_field, {})
    return render_template('reports/summary-table.html', \
                    result=result, this_month = month_name(0), \
                    last_month = month_name(-1), groupField=group_field)

@reportviews.route('/programmes')
@reportviews.route('/programmes/')
def show_programmes():
    result = g.programmes.list()
    return render_template('reports/programmes.html', data=result)

@reportviews.route('/portfolio/<portfolio>')
@reportviews.route('/portfolio/<portfolio>/')
def show_portfolio_summary(portfolio):
    """
    directorate=g.packages.get_directorate_assignments_current(months),
    
    result = g.packages.get_portfolio_assignments_current(portfolio)
    """
    filter = {}
    filter['portfolio'] = portfolio
    result = g.packages.get_portfolio_summary('areaOfWork', filter)
    return render_template('reports/summary-table.html', \
                    result=result, groupField='areaOfWork')

@reportviews.route('/status')
@reportviews.route('/status/')
def show_status_report_table():
    return show_status_report_table_for_month(0) 

@reportviews.route('/status/<chosen_month>')
@reportviews.route('/status/<chosen_month>/')
def show_status_report_table_for_month(chosen_month):
    filter = {}
    month = int(chosen_month)
    workpackages = g.packages.get_wp_list_with_status(filter, month_name(month), month_and_year_name(month-1), month_and_year_name(month))
    months=[]
    for i in range(1,-7,-1):
        month = {}
        month['display'] = month_name_display(i)
        month['month'] = i
        if i==int(chosen_month):
            month['selected'] = "selected"
        else:
            month['selected'] = ""
        months.append(month)
    #print workpackages
    return render_template('reports/status-report-table.html', list=workpackages, months=months, \
                           month_name_display=month_name_display(int(chosen_month)), \
                           month_name=month_name(int(chosen_month)), \
                           month_number=int(chosen_month))
    
@reportviews.route('/latestUpdate/<workpackage_id>')
@reportviews.route('/latestUpdate/<workpackage_id>/')
def get_latest_update(workpackage_id):
    latest = g.packages.get_latest_status_update(workpackage_id)
    return (latest['update'])

@reportviews.route('/csv/status/<chosen_month>')
@reportviews.route('/csv/status/<chosen_month>/')
def status_updates_report_csv(chosen_month):
    
    filter = {}
    month = int(chosen_month)
    data = g.packages.get_wp_list_for_status_report(filter, month_name(month), month_name(month+1), month_name(month+2), month_and_year_name(month-1), month_and_year_name(month))
    csv = ""
    """
    report_csv=['portfolio','areaOfWork','name', 'endDate','status','closedDate']
    
    Other fields:
    "month1","month2","month3","statusUpdate","statusUpdateAuthor","statusUpdateDate","rag","pastEndDate"
    
    """
    
    """
    for k in FIELD_LIST['report_csv']:
        csv = csv + '"' + k + '",'
    """
    csv = csv + '"portfolio","areaOfWork","workpackageName","endDate","status","closedDate","' + month_name(month) + '","' + month_name(month+1) + '","' + month_name(month+2) + \
                    '","statusUpdate","statusUpdateAuthor","statusUpdateDate","rag","pastEndDate"\n'
    
    # Basic WP fields first
    for row in data:
        #if row['latest_update']['update']:
            
        for k in FIELD_LIST['report_csv']:
            if k in row:
                #print type(row[k])
                if type(row[k]) is unicode:
                    csv = csv + '"' + removeNonAscii(row[k]) + '",'
                else:
                    csv = csv + '"' + str(row[k]) + '",'
            else:
                csv = csv + ','
        # Allocations for next 3 months
        csv = csv + '"' + str(row['month0']) + '",'
        csv = csv + '"' + str(row['month1']) + '",'
        csv = csv + '"' + str(row['month2']) + '",'
        
        # Status updates
        if row['month_update']:
            #print row
            if row['month_update']['update']:
                csv = csv + '"' + row['month_update']['update'] + '",'
                csv = csv + '"' + row['month_update']['author'] + '",'
                csv = csv + '"' + row['month_update']['date'].strftime('%d/%m/%Y') + '",'
                csv = csv + '"TBD",'
            else:
                csv = csv + '"","","","",'
        else:
            csv = csv + '"","","","",'
        
        # Past end date
        if row['after_end_date']:
            csv = csv + '"True",'
        else:
            csv = csv + '"",'
        csv = csv + '\n'
    
    headers = {}
    headers['Content-Disposition']='filename="StatusReport-' + month_name(month) + '.csv"'
    return Response(csv, mimetype='text/csv', headers=headers)

@reportviews.route('/includeinreport', methods=['POST'])
def include_in_report():
    # retrieve form data
    data = request.values
    
    wp_id = data['workpackageid']
    month = data['month']
    action = data['action']

    # retrieve work package from rethinkdb
    old_wp = g.packages.get_workpackage(wp_id)
    
    # now, get the updates for this work package ordered by date (latest first), and retrieve the date of the latest update
    latest_update = list(g.packages.get_workpackage_status_updates(wp_id))[0]
    date_of_latest_update = latest_update['StatusUpdates'][0]['date']
    #print "Latest update was on date:"
    #print date_of_latest_update
    
    updates_element = []
    if 'StatusUpdates' in old_wp:
        updates_element = old_wp['StatusUpdates']

    # Loop through until we find the latest update, which we want to add/remove from the report
    for update_entry in updates_element:
        report_tags = update_entry['report_tags']
        if action == 'remove':
            # Remove tag for this month if it is in this update
            if any(tag == month for tag in report_tags):
                new_tag_list = []
                for tag in report_tags:
                    if (tag == month):
                        pass
                    else:
                        new_tag_list.append(tag)
                update_entry['report_tags'] = new_tag_list
                g.packages.update_workpackage_noaudit(wp_id, 'StatusUpdates', updates_element)
            else:
                print "This update was not included for the month specified"
        
        if action == 'add':
            if update_entry['date'] == date_of_latest_update:
            # Add tag for this month
                if any(tag == month for tag in report_tags):
                    print "This update is already included for the month specified"
                else:
                    update_entry['report_tags'].append(month)
                    g.packages.update_workpackage_noaudit(wp_id, 'StatusUpdates', updates_element)
    
    # Return the latest update for this work package, so the view can be updated if required        
    return get_latest_update(wp_id)
