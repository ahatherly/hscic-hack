from __future__ import with_statement
import time
from datetime import datetime, date, timedelta
from dateutil import rrule
from models import User
from flask import g

FIELD_LIST = dict(
                display = ['portfolio', 'portfolioOwner', 'name', 'status', 
                    'areaOfWork', 'id', 'leadPerson', 'level1Type', 'level2Type', 'serviceType',
                    'forecastStartDate', 'forecastEndDate', 'percentageFteEffort'],
                csv_text = ['portfolio', 'programme', 'portfolioOwner', 'name', 'status', 
                    'areaOfWork', 'id', 'leadPerson', 'level1Type', 'level2Type', 'serviceType',
                    'forecastStartDate', 'forecastEndDate', 'percentageFteEffort', 'originator',
                    'path', 'businessCase', 'created'],
                csv_html = ['description', 'deliverables', 'milestones', 'risks', 'stakeholders', 'approvals'],
                query = ['portfolio', 'programme', 'status', 'areaOfWork', 'leadPerson', 'dq_filter', 'level1Type', 'level2Type', 'serviceType'],
                display_person = ['portfolio', 'name', 'status', 
                    'areaOfWork', 'id', 'Assigned'],
                person = ['id', 'email', 'name'],
                assignments=['name', 'status', 'dec12', 'jan13', 'feb13', 'id'],
                allocations=['person', 'workpackage_id', 'start_month', 'end_month', 'status', 'table_id'],
                new=['name'],
                graphs=['month'],
                reports=['group_field'],
                login=['token','username'],
                report_csv=['portfolio','areaOfWork','name','forecastEndDate','status','closedDate']
             )

def clean_string(str):
    return str.replace(u'\u201c', '"') \
            .replace(u'\u201d', '"') \
            .replace(u'\u2018', '\'') \
            .replace(u'\u2019', '\'') \
            .replace(u'\u2014', '-') \
            .replace(u'\u2013', '-')

def get_value_for_display(item, key):
    if not key in item:
        return ""
    else:
        return str(item[key])

def removeNonAscii(s): return "".join(i for i in s if ord(i)<128)

def filter_request_args(args, whitelist):
    """
    Filters list for valid parameters
    """
    cleansed = {}
    for k, v in args.items():
        if (k in whitelist):
            cleansed[k]=v
    return cleansed
    
def month_name(offset):
    today = date.today()
    return monthdelta(today, offset).strftime("%b%y").lower()

def month_and_year_name(offset):
    today = date.today()
    return monthdelta(today, offset).strftime("%m/%Y")

def month_and_year_name_year_first(offset):
    today = date.today()
    return monthdelta(today, offset).strftime("%Y-%m")

def month_name_display(offset):
    today = date.today()
    return monthdelta(today, offset).strftime("%b-%y")

def month_as_full_date(offset):
    today = date.today()
    return monthdelta(today, offset).strftime("01 %b %y")

# Code taken from http://stackoverflow.com/questions/3424899/whats-the-simplest-way-to-subtract-a-month-from-a-date-in-python
def monthdelta(date, delta):
    m, y = (date.month+delta) % 12, date.year + ((date.month)+delta-1) // 12
    if not m: m = 12
    d = min(date.day, [31,
        29 if y%4==0 and not y%400==0 else 28,31,30,31,30,31,31,30,31,30,31][m-1])
    return date.replace(day=d,month=m, year=y)

def timestamp():
    return str(datetime.utcnow())

def formatted_date():
    today = date.today()
    return today.strftime("%d/%m/%Y")

def parse_date(date_val):
    try:
        return datetime.strptime(date_val, "%d/%m/%Y")
    except:
        return None
        
def parse_month_as_date(month):
    """
    Assumes a month in the format jan13
    """
    if len(month) <> 5:
        return None
    try:
        dt = datetime.strptime(month, "%b%y")
        return dt
    except:
        return None
    
def date_to_month(date_val):
    return date_val.strftime("%b%y").lower()

def first_day_of_month(month):
    """
    Assumes a month in the format jan13
    """
    if len(month) <> 5:
        return None
    try:
        dt = datetime.strptime(month, "%b%y")
        return dt.replace(day=1)
    except:
        return None

def last_day_of_month(month):
    """
    Assumes a month in the format jan13
    """
    if len(month) <> 5:
        return None
    try:
        dt = datetime.strptime(month, "%b%y")
        if dt.month == 12:
            return dt.replace(day=31)
        else:
            return dt.replace(month=dt.month+1, day=1) - timedelta(days=1)
    except:
        return None

def business_days_in_month(month):
    first = first_day_of_month(month)
    last = last_day_of_month(month)
    return business_days(first, last)

def business_days(start_date, end_date):
    dates=rrule.rruleset()
    dates.rrule(rrule.rrule(rrule.DAILY, dtstart=start_date, until=end_date)) 
             # this set is INCLUSIVE of start_date and end_date 
    dates.exrule(rrule.rrule(rrule.DAILY, 
                            byweekday=(rrule.SA, rrule.SU), 
                            dtstart=start_date)) 
            # here's where we exclude the weekend dates 
    working_days = len(list(dates))
    return working_days

def truncate_strings_in_list(lst, length):
    new_list = []
    for item in lst:
        new_list.append((item[:length] + '..') if len(item) > length else item)
    return new_list

def formatAllocationsAsHTML(allocations):
    result = "<ul>"
    # First, sort by person
    sortedFrom = sorted(allocations, key=lambda personEntry: personEntry['Name'])
    # Now sort the allocation entries by date
    for personEntry in sortedFrom:
        allocationEntries = personEntry['Allocations']
        allocationMonthList = [parse_month_as_date(month) for month in allocationEntries.keys()]
        sortedMonthList = sorted(allocationMonthList)
        newAllocationEntries = {}
        result = result + '<li>' + personEntry['Name'] + ' allocations: '
        for monthVal in sortedMonthList:
            result = result + date_to_month(monthVal) + ' = ' + str(allocationEntries[date_to_month(monthVal)]) + '  '
        result = result + '</li>'
    return result

class Timer(object):
    """
    For timing functions
    
    Example:
        
        timer = Timer()
        
        with timer:
           *thing to measure*
        
        print timer.duration_in_seconds()
    """
    
    def __enter__(self):
        self.__start = time.time()

    def __exit__(self, *args):
        self.__finish = time.time()

    def duration_in_seconds(self):
        return self.__finish - self.__start

