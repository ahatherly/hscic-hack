"""
View logic for people
"""
from flask import Blueprint, render_template, request, g
from flask_login import current_user
from app.datastores.packages import PackagesStore
from app.datastores.people import PeopleStore
from app.models import User
from app import app, lm
import json

peopleviews = Blueprint('peopleviews', __name__,
                        template_folder='templates')

@peopleviews.before_request
def before_request():
    if not current_user.is_authenticated:
        return lm.unauthorized()
    g.packages = PackagesStore(g.rdb_conn)
    g.people = PeopleStore(g.rdb_conn)


@peopleviews.route('/')
def show_noperson_assignment_html():
    """ 
    Show the list of all work packages for current user
    """
    return list_assignments(request.args.get('person', g.user.name))

@peopleviews.route('/lists/<list_name>')
@peopleviews.route('/lists/<list_name>/')
def get_lookup_list(list_name):
    """
    Returns distinct key/values for people using the given field    
    """
    return json.dumps(g.people.get_list_values(list_name))

@peopleviews.route('/<name>')
def list_assignments(name):
    """ 
    Show the list of all work packages
    """
    assignments = list(g.packages.get_assignments(name))

    if len(assignments)==0:
        assignments.append({'portfolio':'No data', 
                            'name':'No data', 
                            'status':'Active', 
                            'areaOfWork':'No data', 
                            'id':'No data', 
                            'Assigned':'No data'})
 
    # Get the actual person record from the user table
    db_user = g.people.one_by_name(name)
    
    if (db_user.emailauth != None):
        gravatar_url = User.avatar(128, db_user.emailauth)
    else:
        gravatar_url = User.avatar(128, db_user.email)

    cols = app.config['ALLOCATION_COLUMNS_PERSON_PAGE']
    start_month = app.config['ALLOCATION_START_OFFSET_PERSON_PAGE']
    end_month = start_month + cols - 1
    
    return render_template('people/panel-people43.html',
                           start_month=start_month, end_month=end_month,
                           user=db_user,
                           user_name=name, 
                           assignments=assignments, \
	                       people=g.people.get_all_names(), 
                           gravurl=gravatar_url)

@peopleviews.route('/isadmin')
def is_admin():
    if current_user.is_admin():
        return "True"
    else:
        return "False"

