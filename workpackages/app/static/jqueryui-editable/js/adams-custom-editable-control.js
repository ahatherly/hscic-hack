/**
SelectAH (autocomplete box)

@class SelectAH
@extends list
@final
@example
<a href="#" id="status" data-type="SelectAH" data-pk="1" data-url="/post" data-original-title="Select status"></a>
<script>
$(function(){
    $('#status').editable({
        value: 2,    
        source: [
              {value: 1, text: 'Active'},
              {value: 2, text: 'Blocked'},
              {value: 3, text: 'Deleted'}
           ]
    });
});
</script>
**/
(function ($) {
    "use strict";
    
    var SelectAH = function (options) {
    	this.first_x = -1;
    	if ('submit_first_x_chars' in options) {
    		this.first_x = options['submit_first_x_chars'];
    	}
        this.init('SelectAH', options, SelectAH.defaults);
    };

    $.fn.editableutils.inherit(SelectAH, $.fn.editabletypes.list);

    $.extend(SelectAH.prototype, {
        renderList: function() {
            this.$input.empty();
            var first_x = this.first_x;

            var getItemArray = function(data) {
            	var vals = [];
            	if($.isArray(data)) {
                    for(var i=0; i<data.length; i++) {
                        vals.push(data[i].text); 
                    }
                }
            	return vals;
            }
            
            var processClick = function(event, ui) {
            	// Mouse click or enter pressed
            	if (event.which == 1 || event.which == 13) {
            		var val = ui.item.value;
            		
            		// See if we want to submit just the first X chars as the value. Otherwise, just submit the whole value
            		if (first_x > 0) {
	            		if (val.length > 8)
	            			val = val.substring(0,8);
            		}
            		
            		$(this).val(val);
            		$(this).closest('form').submit();
            	}
            }
            
            this.$input.autocomplete({
                source: getItemArray(this.sourceData), minLength: 0,
                select: processClick
            }).data("ui-autocomplete")._renderItem = function( ul, item) {
            		return $( "<li>" )
	                .append( "<a>" + item.value + "</a>" )
	                .appendTo( ul );
	            };
            
            this.setClass();
        },
       
        value2htmlFinal: function(value, element) {
        	$(element).text(value);
        },
        
        autosubmit: function() {
            this.$input.off('keydown.editable').on('change.editable', function(){
                $(this).closest('form').submit();
            });
        }
    });      

    SelectAH.defaults = $.extend({}, $.fn.editabletypes.list.defaults, {
        /**
        @property tpl 
        @default <select></select>
        **/         
        tpl:'<input type="text">',
    	/**
        @property submit_first_x_chars 
        @default -1
        **/         
        submit_first_x_chars:-1
    });

    $.fn.editabletypes.SelectAH = SelectAH;      

}(window.jQuery));
