$(function(){
        var derivers = $.pivotUtilities.derivers;

        var renderers = $.extend(
            $.pivotUtilities.renderers, 
            $.pivotUtilities.c3_renderers, 
            $.pivotUtilities.d3_renderers, 
            $.pivotUtilities.export_renderers
            );

        $.getJSON("/reports/pivot/", function(items) {
            $("#pivot").pivotUI(items, {
                renderers: renderers,
                /*derivedAttributes: {
                    "Age Bin": derivers.bin("Age", 10),
                    "Gender Imbalance": function(mp) {
                        return mp["Gender"] == "Male" ? 1 : -1;
                    }
                },*/
                cols: ["AllocationMonth", "DaysInMonth"],
                rows: ["AllocatedPerson"],
                vals: ["AllocationEffort"],
                aggregatorName: "Sum",
                rendererName: "Heatmap"
            });
        });
});