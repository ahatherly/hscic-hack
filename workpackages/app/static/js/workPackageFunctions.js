/* Javascript used on work package details page */
$(document).ready(function(){
	load_status_updates();
	display_months();
	calculate_mandays();
});

/*
 * Functions called when specific fields are updated
 */
function load_status_updates() {
	var id = $("#workpackageid").val();
	$("#status_updates_tab").load("/workpackages/"+id+"/status_updates/");
}

function wp_name_change(response, newValue) {
	// Update name of work package on all panels
	//$('#item-name-heading').text(newValue);
	$(".item-name-heading").each(function() { $(this).text(newValue) })
}

function create_new_wp() {
	wp_name = $('#new_wp_name').val();
	$.ajax({
		url:'/workpackages/check/?name='+wp_name,
		type: "GET",
		success: function(response) {
			if (response=='DUPLICATE') {
				$('#errorModalContent').text('There is already a work package with that name!');
				$('#errorModal').foundation('reveal', 'open');
			} else {
				$('#new_wp_form').submit();
			}
		},
		error: function(response) {
			$('#errorModalContent').text('There was an error creating the work package - please try again.');
			$('#errorModal').foundation('reveal', 'open');
		}
	})
}

function wp_start_date_changed(response, newValue) {
	start_date = newValue;
	end_date = parseUKDate($('#forecastEndDate').text());
	percentageFTEeffort = $('#percentageFteEffort').text();
	recalculate_mandays(start_date, end_date, percentageFTEeffort);
}
function wp_end_date_changed(response, newValue) {
	start_date = parseUKDate($('#forecastStartDate').text());
	end_date = newValue;
	percentageFTEeffort = $('#percentageFteEffort').text();
	recalculate_mandays(start_date, end_date, percentageFTEeffort);
}
function wp_effort_changed(response, newValue) {
	start_date = parseUKDate($('#forecastStartDate').text());
	end_date = parseUKDate($('#forecastEndDate').text());
	percentageFTEeffort = newValue;
	recalculate_mandays(start_date, end_date, percentageFTEeffort);
}
function calculate_mandays() {
	start_date = parseUKDate($('#forecastStartDate').text());
	end_date = parseUKDate($('#forecastEndDate').text());
	percentageFTEeffort = $('#percentageFteEffort').text();
	recalculate_mandays(start_date, end_date, percentageFTEeffort);
}

function recalculate_mandays(start_date, end_date, percentageFTEeffort) {
	//alert(start_date+", "+end_date+", "+percentageFTEeffort);
	if (start_date == null || end_date == null || !isNumber(percentageFTEeffort)) {
		$('#calculatedEffort').html("");
		return;
	}
	total_mandays = workingDaysBetweenDates(start_date, end_date);
	mandays = Math.ceil(total_mandays * (percentageFTEeffort/100)); 
	tooltip = "Working days between " + 
				$.datepicker.formatDate('dd/mm/yy', start_date) + " and " +
				$.datepicker.formatDate('dd/mm/yy', end_date) + " is " + total_mandays +
				" * " + percentageFTEeffort + "% = " + mandays;
	message="<strong>Estimated Mandays:</strong> " + mandays + "&nbsp;&nbsp;&nbsp;&nbsp;" +
					"<small><span data-tooltip class='has-tip' title='" + tooltip + "'>why?</span></small>";
	$('#calculatedEffort').html(message);
}

/*
 * Monthly allocations table
 */
function display_months() {
	var id = $("#workpackageid").val();
	var start_month = $("#start_month").val();
	var end_month = $("#end_month").val();
	$("#workpackage_allocations_table").load("/allocations/view/?workpackage_id="+id+"&start_month="+start_month+"&end_month="+end_month);
}

function update_months(start_month, end_month) {
	var id = $("#workpackageid").val();
	$("#workpackage_allocations_table").load("/allocations/view/?workpackage_id="+id+"&start_month=" + start_month + "&end_month=" + end_month);
}

/*
 * Adding a new allocated person
 */
function addAllocation() {
	var id = $("#workpackageid").val();
	// read value
	var x = $("#addAllocation").val();
	// check if not null
	if (x.length>0)
	{					
		// save it
		$.ajax({
			url:'/allocations/saveallocation/',
			type: "POST",
			data: 'workpackageid='+id+'&month=apr13&user=' + x + '&value=0',
			success: function(response) {
				// call to update the allocations display
				display_months();
			},
			error: function(response) {
				$('#errorModalContent').text('Failed to add person');
				$('#errorModal').foundation('reveal', 'open');
			}
		})			
		
	}
}

//Called when the status updates modal is closed
function refresh_after_status_update_modal_closed(workpackage_id, newValue) {
	load_status_updates();
}