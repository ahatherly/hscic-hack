// Javascript date processing is very browser dependent, to these functions try to make this consistent

// Taken from: http://partialclass.blogspot.co.uk/2011/07/calculating-working-days-between-two.html
function workingDaysBetweenDates(startDate, endDate) {
  
    // Validate input
    if (endDate < startDate)
        return 0;
    
    // Calculate days between dates
    var millisecondsPerDay = 86400 * 1000; // Day in milliseconds
    startDate.setHours(0,0,0,1);  // Start just after midnight
    endDate.setHours(22,59,59,999);  // End just before midnight (taking into account clock changes)
    
    var diff = endDate - startDate;  // Milliseconds between datetime objects    
    var days = Math.ceil(diff / millisecondsPerDay);
    
    // Subtract two weekend days for every week in between
    var weeks = Math.floor(days / 7);
    var days = days - (weeks * 2);

    // Handle special cases
    var startDay = startDate.getDay();
    var endDay = endDate.getDay();
    
    // Remove weekend not previously removed.   
    if (startDay - endDay > 1)         
        days = days - 2;      
    
    // Remove start day if span starts on Sunday but ends before Saturday
    if (startDay == 0 && endDay != 6)
        days = days - 1
        
    // Remove end day if span ends on Saturday but starts after Sunday
    if (endDay == 6 && startDay != 0)
        days = days - 1
    
    return days;
}

// Parse a date in dd/mm/yyyy format - variation of http://stackoverflow.com/questions/2587345/javascript-date-parse
function parseUKDate(input) {
  if (input.length != 10)
	  return null;
  var parts = input.split('/');
  if (parts.length != 3)
	  return null;
  return new Date(parts[2], parts[1]-1, parts[0]); // months are 0-based
}

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

// Takes a month in the format: Jul-13 and returns the number of working days in the month
function workingDaysInMonth(month) {
	if (month === undefined)
		return "";
	if (month.length != 6)
		return "";
	firstDay = firstDayOfMonth(month);
	lastDay = lastDayOfMonth(month);
	return workingDaysBetweenDates(firstDay, lastDay);
}

// Takes a month in the format: Jul-13 and returns a javascript date object representing the first day in that month
function firstDayOfMonth(input) {
	if (input.length != 6)
		  return null;
	var parts = input.split('-');
	if (parts.length != 2)
		  return null;
	month_number = toMonthNumber(parts[0]);
	year = "20"+parts[1];
	return new Date(year, month_number, 1);
}

// Takes a month in the format: Jul-13 and returns a javascript date object representing the last day in that month
// Last line taken from: http://stackoverflow.com/questions/222309/calculate-last-day-of-month-in-javascript
function lastDayOfMonth(input) {
	if (input.length != 6)
		  return null;
	var parts = input.split('-');
	if (parts.length != 2)
		  return null;
	month_number = toMonthNumber(parts[0]);
	year = "20"+parts[1];
	return new Date(year, month_number + 1, 0);
}

// Takes a three character month name and turns it into a month number (Jan=0, Feb=1, etc)
function toMonthNumber(month) {
	switch(month.toLowerCase()) {
		case 'jan': return 0;
		case 'feb': return 1;
		case 'mar': return 2;
		case 'apr': return 3;
		case 'may': return 4;
		case 'jun': return 5;
		case 'jul': return 6;
		case 'aug': return 7;
		case 'sep': return 8;
		case 'oct': return 9;
		case 'nov': return 10;
		case 'dec': return 11;
	}
}