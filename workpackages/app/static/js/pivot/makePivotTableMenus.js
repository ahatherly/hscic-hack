<!--
This is not currently used - was an attempt to do a clever drop-downs for selecting columns in the pivot table
I may resurrect it at some point...
Adam
-->
function addMenus(fields) {
	for (var i=0; i<fields.length; i++) {
		fieldName = fields[i]['name'];
		$("#dropdown-field-list-values").append("<li><label><input type=\"checkbox\" name=\"" + fieldName + "\" onclick=\"checkUncheck(this)\"> " + fieldName + "</label></li>");
	}
}

function checkUncheck(field) {
	alert(field.name + " : " + field.checked);
}


<!--
To go at end of html page:
	<div id="dropdown-field-list" class="dropdown dropdown-tip">
		<ul class="dropdown-menu" id="dropdown-field-list-values"></ul>
	</div>
	
To go where you want the dropdown:
	<a href="#" data-dropdown="#dropdown-field-list">Choose columns</a>

To call on doc load:
	addMenus(field_definitions);
	
Also needs jquery.dropdown.js and jquery.dropdown.css
-->