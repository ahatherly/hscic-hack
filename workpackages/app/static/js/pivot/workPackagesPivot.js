function setupPivot(input){
    input.callbacks = {afterUpdateResults: function(){
      $('#results > table').dataTable({
        "sDom": "<'row'<'span6'l><'span6'f>>t<'row'<'span6'i><'span6'p>>",
        "iDisplayLength": 25,
        "aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
        "oLanguage": {
          "sLengthMenu": "_MENU_ records per page"
        }
      });
    }};
    $('#pivot-menu-container').pivot_display('setup', input);
};

function pivotLoad() {
	$.getJSON('/reports/pivot/', function(data) {
		  setupPivot({json:data,fields:field_definitions,
			  rowLabels:["portfolio", "areaOfWork"],
			  columnLabels:["allocation_month"],
			  summaries:["mandaysAllocated"],
			  filters:{status:'Active'}});
	});
}

// Canned reports
function updatedCannedReports(event) {
	name = $(this).val()
	if (name == 'Portfolio') {
		$('#pivot-menu-container').pivot_display('reprocess_display', {
	    	  rowLabels:["portfolio"],
	    	  columnLabels:["allocation_month"],
			  summaries:["mandaysAllocated"],
			  filters:{status:'Active'}
		})
	} else if (name == 'Person') {
		$('#pivot-menu-container').pivot_display('reprocess_display', {
	    	  rowLabels:["allocatedPerson"],
	    	  columnLabels:["allocation_month"],
			  summaries:["mandaysAllocated"],
			  filters:{status:'Active'}
		})
	} else if (name == 'ServiceType') {
		$('#pivot-menu-container').pivot_display('reprocess_display', {
	    	  rowLabels:["serviceType"],
	    	  columnLabels:["allocation_month"],
			  summaries:["mandaysAllocated"],
			  filters:{status:'Active'}
		})
	}
}

// Field Definitions for Pivot
var field_definitions = [
     {name: 'name',   type: 'string',   filterable: true},
     {name: 'status',        type: 'string',   filterable: true},
     {name: 'programme',        type: 'string',   filterable: true},
     {name: 'portfolio',        type: 'string',   filterable: true},
     {name: 'portfolioOwner',        type: 'string',   filterable: true},
     {name: 'areaOfWork',        type: 'string',   filterable: true},
     {name: 'serviceType',        type: 'string',   filterable: true},
     {name: 'level1Type',        type: 'string',   filterable: true},
     {name: 'level2Type',        type: 'string',   filterable: true},
     {name: 'percentageFteEffort',        type: 'integer',   filterable: false},
     {name: 'forecastStartDate',        type: 'string',   filterable: false},
     {name: 'forecastEndDate',        type: 'string',   filterable: false},
     {name: 'leadPerson',        type: 'string',   filterable: true},
     {name: 'allocatedPerson',        type: 'string',   filterable: true},
     
     {name: 'allocation_month', type: 'string', filterable: true, pseudo: true, columnLabelable: true,
			pseudoFunction: function(row){ 
				var date = new Date(row.month);
				// NOTE: Nasty hack alert.. pivot.js column sorting is only based on alphanumeric
				// ordering, so we need to do this to ensure our months are in order despite being
				// alpha month names
				return ("<span style='display:none'>" + $.datepicker.formatDate('yy mm', date) + "</span>" + $.datepicker.formatDate('M yy', date))
		    }
	 },
       
     // summary fields
     {name: 'mandaysAllocated',        	type: 'integer', rowLabelable: false, summarizable: 'sum',
        	 						displayFunction: displayIfNumber },
     {name: 'workpackageCount',        	type: 'integer', rowLabelable: false, summarizable: 'sum',
			displayFunction: displayIfNumber }
];

function displayIfNumber(value) {
	if (isNaN(value)) {
		return "";
	} else {
		return value;
	}
}
