import rethinkdb as r
from app.utils import *
from app import app
from app.datastores.datastore import DataStore
from app.datastores.packagefilters import PackageFilters
from app.datastores.reference import ReferenceStore

import functools, inspect, datetime, copy

from app.cachestore import CacheStore

class PackagesStore(DataStore):
    """
    Implementation of store of packages
    """

    def __init__(self, rdb_conn, packagesTable="packages"):
        DataStore.__init__(self, rdb_conn, packagesTable)
        g.reference = ReferenceStore(g.rdb_conn)

    def get_wp_list_with_status(self, filter_output, month_name, monthyear1, monthyear2):
        query = PackageFilters(g.packages).order_by_multiple('portfolio','areaOfWork','name') \
                            .filter(filter_output) \
                            .active_or_closed_in_months(monthyear1, monthyear2) \
                            .add_latest_status_update() \
                            .add_status_update_for_month(month_name)
        return query.execute().val()
    
    def get_latest_status_update(self, workpackage_id):
        query = PackageFilters(g.packages).by_id(workpackage_id).add_latest_status_update()
        result = query.execute().val()
        return result[0]['latest_update']
    
    def get_wp_list_for_status_report(self, filter_output, month_name, month_name2, month_name3, monthyear1, monthyear2):
        query = PackageFilters(g.packages).order_by_multiple('portfolio','areaOfWork','name') \
                            .filter(filter_output) \
                            .active_or_closed_in_months(monthyear1, monthyear2) \
                            .add_status_update_for_month(month_name) \
                            .add_total_allocation_for_month_with_named_output(month_name, "month0") \
                            .add_total_allocation_for_month_with_named_output(month_name2, "month1") \
                            .add_total_allocation_for_month_with_named_output(month_name3, "month2")
        return query.execute().add_expected_effort_and_date_checks().val()
    
    def get_wp_list(self, filter_output, dq_filter):
        # now run the query with or without a filter if initial page load
        
        query = PackageFilters(g.packages).order_by_multiple('portfolio','areaOfWork').filter(filter_output)
        result = ""

        """
        Apply data quality filters
        """
        if dq_filter == 'unallocated':
            result = query.add_allocation_fields().filter_by_field('count_of_allocated_resources',0) \
                    .execute().val()
        elif dq_filter == 'no_programme':
            result = query.filter_by_field_not_exists_or_blank('programme').execute().val()
        elif dq_filter == 'no_activity':
            result = query.add_allocation_fields().greater_than('count_of_allocated_resources',0) \
                    .filter_by_field('total_allocation',0).execute().val()
        elif dq_filter == 'missing_dates':
            result = query.execute().add_expected_effort_and_date_checks() \
                    .filter_dict('missing_fields', True).val()
        elif dq_filter == 'past_end_date':
            result = query.execute().add_expected_effort_and_date_checks() \
                    .filter_dict('after_end_date', True).val()
        elif dq_filter == 'before_start_date':
            result = query.execute().add_expected_effort_and_date_checks() \
                    .filter_dict('before_start_date', True).val()
        elif dq_filter == 'under_allocated':
            result = query.add_allocation_fields().execute().add_expected_effort_and_date_checks() \
                    .filter_dict('under_allocated', True).val()
        elif dq_filter == 'over_allocated':
            result = query.add_allocation_fields().execute().add_expected_effort_and_date_checks() \
                    .filter_dict('over_allocated', True).val()
        else:
            result = query.execute().val()
            
        return result
    
    """
    
    NOTE!
    
    If we want to update certain fields to use a pre-defined pick-list rather than just rely on
    the previously entered values, we can simply update the two below methods
    
    """
    @CacheStore(u"portfolios", 10, vary_by=[1])
    def get_list(self, field):
        """
        Returns distinct list of values in field
        """
        
        if field in app.config['CONSTRAINED_FIELDS']:
            list = g.reference.get_values(field)
            if list == None:
                return []
            elif len(list) == 0:
                return []
            else:
                return [item for item in list['field_values']]
        else:
            list = self.query(
                self.all() \
                    .filter(r.row['status'].ne("Deleted")) \
                    .pluck(field) \
                    .distinct() \
                    .has_fields(field))
            if list == None:
                return []
            elif len(list) == 0:
                return []
            else:
                return [item[field] for item in list]
        
    @CacheStore(u"get_list_values", 1, vary_by=[1])
    def get_list_values(self, field_name):
        
        if field_name in app.config['CONSTRAINED_FIELDS']:
            list_values = g.reference.get_values(field_name)
            """
            Check if this is empty and return an empty list to avoid our cache blowing up
            """
            if (list_values == None):
                return [{u'value': u'', u'text': u''}]
            elif len(list_values) == 1:
                if (list_values[0] == {}):
                    return [{u'value': u'', u'text': u''}];
            """
            Return the actual list of values
            """
            return [{u'value': list_value, 
                     u'text': list_value} 
                    for list_value in list_values['field_values']]
        else:
            list_values = self.query(self.all().filter(r.row['status'].ne("Deleted")).pluck(field_name).distinct().has_fields(field_name))
            #print list_values
            """
            Check if this is empty and return an empty list to avoid our cache blowing up
            """
            if len(list_values) == 1:
                if (list_values[0] == {}):
                    return [{u'value': u'', u'text': u''}];
            """
            Return the actual list of values
            """
            return [{u'value': list_value[field_name], 
                     u'text': list_value[field_name]} 
                    for list_value in list_values]

    def get_words_for_analysis(self,field):
	query = self.all() \
		.has_fields(field) \
		.map(lambda wp:wp[field])
	return self.query(query)
    
    def get_assignments(self, name):
        """
        Returns distinct list of portfolios
        """
        query = self.all() \
            .filter(r.row['status'].ne("Deleted")) \
            .filter(r.row['Assigned'] \
            .filter(lambda wp:wp['Name']==name) \
            .count()>0).order_by('name') \
            .pluck(*FIELD_LIST['display']).has_fields('Assigned', 'Name')
        return self.query(query)
    
    @CacheStore(u"get_assignments_current", 10, vary_by=[1,2])
    def get_assignments_current(self, name, months):
        """
        Returns distinct list of portfolios
        """
        months1=months['month1']
        months2=months['month2']
        months3=months['month3']

        def extract_individual_assignments(assigned):
            alloc = assigned['Assigned'].distinct().filter(lambda wp:wp['Name']==name).map(map_months)
            return dict(name=assigned['name'], uuid=assigned['id'], allocations=alloc)

        def map_months(assigned):
            month1 = assigned['Allocations'][months1].default(0)
            month2 = assigned['Allocations'][months2].default(0)
            month3 = assigned['Allocations'][months3].default(0)
            status=assigned['Status']
            return dict(status=status, month1=month1, month2=month2, month3=month3)

        return self.query(self.all() \
            .filter(r.row['status'].ne("Deleted")) \
            .filter(r.row['Assigned'] \
            .filter(lambda wp:wp['Name']==name) \
            .count()>0).order_by('name') \
            .map(extract_individual_assignments))
    
    def get_total_allocation_for_person(self, wp_id, user):
        query = PackageFilters(g.packages).by_id(wp_id).add_total_allocation_for_person(user).execute().val()
        return query
    
    def add_workpackage(self, fieldname, newvalue):
        """
        Adds workpackage
        """
        guid = self.post({fieldname:newvalue})
        return guid
    
    def new_workpackage(self, newwp):
        """
        New workpackage
        """
        guid = self.post(newwp)
        return guid

    def update_workpackage(self, workpackageid, fieldname, newvalue):
        """
        Updates workpackage
        """
        self.put(workpackageid, {fieldname:newvalue})
        return 'Updated'
    
    def update_workpackage_noaudit(self, workpackageid, fieldname, newvalue):
        """
        Updates workpackage
        """
        self.put_noaudit(workpackageid, {fieldname:newvalue})
        return 'Updated'

    def update_allocation(self, workpackageid, update):
        """
        Updates workpackage allocation
        """
        return self.put(workpackageid, update)

    def get_workpackage(self, id):
        """
        Return single workpackage
        """
        return self.get(id)
    
    def get_workpackage_by_name(self, name):
        result = PackageFilters(g.packages) \
                    .filter_by_field('name', name) \
                    .execute().val()
        return result
    
    def get_workpackage_status_updates(self, id):
        """
        Return status updates for the workpackage, in descending date order
        """
        def sort_updates_by_date(wp):
            default_update = {'date':None,'update':None,'author':None,'report_tags':[]}
            update_list = wp['StatusUpdates'].default([default_update]).order_by(r.desc('date'))
            return dict(id=wp['id'], name=wp['name'], StatusUpdates=update_list)
        
        return self.query( \
                      self.all() \
                      .filter({'id':id}) \
                      .map(sort_updates_by_date))

    #@CacheStore(u"get_allocations_person", 10, vary_by=[1,2,3])
    def get_allocations_person(self, person_name, month_list, status):
        """
        Returns the allocations for a person for the months specified
        """
        def extract_individual_assignments(assigned):
            alloc = assigned['Assigned'].distinct().filter(lambda wp:wp['Name']==person_name).map(map_months)
            #latest_update = ""
            #if assigned.has_fields('StatusUpdates') == "True":
            default_update = {'date':None,'update':None,'author':None,'report_tags':[]}
            latest_update = assigned['StatusUpdates'].default([default_update]).order_by(r.desc('date')).nth(0)
            
            # See if we are past the end date
            #end_date = assigned['forecastEndDate']
            """
            now = datetime.datetime.now()
            end_date = parse_date(assigned['forecastEndDate'])
            after_end_date = False
            if end_date != None:
                if now > end_date:
                    after_end_date = True
            """
            return dict(name=assigned['name'], uuid=assigned['id'], status=assigned['status'], \
                        allocations=alloc, latest_update=latest_update \
                        , forecastEndDate=assigned['forecastEndDate'].default(None) \
                        #, after_end_date=after_end_date \
                        )
            

        def map_months(assigned):
            month_value = []
            for month_name in month_list:
                month_value.append(assigned['Allocations'][month_name].default(0))
                
            total = assigned['Allocations'].coerce_to('array') \
                            .map(lambda valList: valList.nth(1)) \
                            .reduce(lambda acc, val: acc + val).default(0)
            # NOTE: Added .default(0) above to make it work in new rethinkdb version
            
            monthsWithData = assigned['Allocations'].keys()
                            
            return dict(months=month_value, total=total, monthsWithData=monthsWithData, personStatus=assigned['Status'])

        if status == None:
            # By default show assignments for all non closed and non deleted work packages
            """
            JS Equivalent:
            r.db('workpackages').table('packages')
                                    .filter(r.row('status').ne('Deleted'))
                                    .filter(r.row('status').ne('Closed'))
                                    .filter(r.row('Assigned')
                                                .filter(function(wp){return wp('Name').eq('Adam Hatherly')})
                                                .count().gt(0)
                                    ).orderBy('name')
                                    .map(  ...  )
            """            
            return self.query( \
                      self.all() \
                      .filter(r.row['status'].ne("Deleted")) \
                      .filter(r.row['status'].ne("Closed")) \
                      .filter(r.row['Assigned'] \
                        .filter(lambda wp:wp['Name']==person_name) \
                        .count()>0 \
                      ).order_by('name') \
                      .map(extract_individual_assignments))
        else:
            return self.query( \
                      self.all() \
                      .filter({'status':status}) \
                      .filter(r.row['Assigned'] \
                        .filter(lambda wp:wp['Name']==person_name) \
                        .count()>0 \
                      ).order_by('name') \
                      .map(extract_individual_assignments))
    
    
    #@cache_store(u"get_allocations_all", 10, vary_by=[1])
    def get_allocations_all(self, month_list):
        """
        Returns the allocations for a person for the months specified
        """
        def extract_individual_assignments(assigned):
            alloc = assigned['Assigned'].distinct().map(map_months)
            return dict(wp=assigned.without('Assigned'),
                        allocations=alloc)

        def map_months(assigned):
            month_value = []
            for month_name in month_list:
                month_value.append(assigned['Allocations'][month_name].default(0))
            Status=assigned['Status']
            Name=assigned['Name']
            return dict(Status=Status, Name=Name, months=month_value)
        
        return self.query( \
                  self.all() \
                  .filter(r.row['status'].ne("Deleted")) \
                  .filter(r.row['Assigned'].count()>0) \
                  .order_by('name') \
                  .map(extract_individual_assignments))
    
    #TODO - reinstate after creating a cache invalidator following an add / update
    #@CacheStore(u"get_allocations_wp", 10, vary_by=[1,2])
    def get_allocations_wp(self, workpackage_id, month_list):
        """
        Returns the allocations for a work package for the months specified
        """
        def extract_individual_assignments(assigned):
            alloc = assigned['Assigned'].distinct().order_by('Name').map(map_months)
            
            return dict(name=assigned['name'], uuid=assigned['id'], allocations=alloc)

        def map_months(assigned):
            month_value = []
            for month_name in month_list:
                month_value.append(assigned['Allocations'][month_name].default(0))
            status=assigned['Status']
            name=assigned['Name']
            
            total = assigned['Allocations'].coerce_to('array') \
                            .map(lambda valList: valList.nth(1)) \
                            .reduce(lambda acc, val: acc + val).default(0)
            # Fixed above reduce for new rethinkdb version
            
            monthsWithData = assigned['Allocations'].keys()
            
            return dict(name=name, status=status, months=month_value, total=total, monthsWithData=monthsWithData, personStatus=assigned['Status'])

        query = self.all() \
                .get_all(workpackage_id) \
                .filter(r.row['status'].ne("Deleted")) \
                .map(extract_individual_assignments)
        
        result = list(self.query(query))
        return result

    #@CacheStore(u"get_total_allocations_for_month", 10, vary_by=[1])
    def get_total_allocations_for_month(self, month):
        result = PackageFilters(g.packages) \
                    .add_total_allocation_for_month(month) \
                    .order_by_multiple('portfolio', 'domain') \
                    .execute().val()
        return result

    @CacheStore(u"get_total_allocation_for_month_by_portfolio", 10, vary_by=[1])
    def get_total_allocation_for_month_by_portfolio(self, month):
        result = PackageFilters(g.packages) \
                    .add_total_allocation_for_month(month) \
                    .sum_for_group('portfolio', 'total_allocation_for_month') \
                    .execute().val()
        return result
    
    @CacheStore(u"get_total_allocation_for_month_by_portfolio_and_domain", 10, vary_by=[1])
    def get_total_allocation_for_month_by_portfolio_and_domain(self, month):
        result = PackageFilters(g.packages) \
                    .add_total_allocation_for_month(month) \
                    .sum_for_group_multiple('total_allocation_for_month', 'portfolio', 'areaOfWork') \
                    .execute().val()
        return result
    
    @CacheStore(u"get_count_for_month_by_portfolio_and_domain", 10, vary_by=[1])
    def get_count_for_month_by_portfolio_and_domain(self, month):
        result = PackageFilters(g.packages) \
                    .count_by_group('portfolio', 'areaOfWork') \
                    .execute().val()
        return result
    
    def get_unassigned(self):
        return PackageFilters(g.packages).add_allocation_fields() \
                    .filter_by_field('count_of_allocated_resources',0) \
                    .pluck(FIELD_LIST['display']).execute().val()

    @CacheStore(u"get_portfolio_summary", 10, vary_by=[1,2])
    def get_portfolio_summary(self, group_field, result_filter):
        results = {}
        """
        Some of the figures we can calculate directly in rethinkdb: 
        """
        temp = PackageFilters(g.packages).filter(result_filter).active() \
                    .count_by_group(group_field) \
                    .execute()
        results['total_active'] = temp.reformat_counts().val()
        
        
        results['total_defining'] = PackageFilters(g.packages).filter(result_filter).defining() \
                    .count_by_group(group_field) \
                    .execute() \
                    .reformat_counts() \
                    .val()
                    
        results['total_on_hold'] = PackageFilters(g.packages).filter(result_filter).on_hold() \
                    .count_by_group(group_field) \
                    .execute() \
                    .reformat_counts() \
                    .val()
        
        """
        Others we have to do in python because of the complex date manipulations
        required which are not currently supported in rethinkdb
        """
        this_month = month_name(0)
        last_month = month_name(-1)
        
        this_month_data = PackageFilters(g.packages).filter(result_filter).active().add_total_allocation_for_month(this_month).execute() \
                                .add_expected_effort_for_month(this_month).val()
        last_month_data = PackageFilters(g.packages).filter(result_filter).active().add_total_allocation_for_month(last_month).execute() \
                                .add_expected_effort_for_month(last_month).val()
        
        fte_planned_this_month = {}
        fte_planned_last_month = {}
        fte_actual_this_month = {}
        fte_actual_last_month = {}
        started_this_month = {}
        started_last_month = {}
        ended_this_month = {}
        ended_last_month = {}
        closed_this_month = {}
        closed_last_month = {}
        missing_fields = {}
        
        for item in this_month_data:
            try:
                group = item[group_field]
            except:
                group = ""
            # Initialise counters to zero
            if group not in fte_planned_this_month:
                fte_planned_this_month[group] = 0
                fte_planned_last_month[group] = 0
                fte_actual_this_month[group] = 0
                fte_actual_last_month[group] = 0
                started_this_month[group] = 0
                started_last_month[group] = 0
                ended_this_month[group] = 0
                ended_last_month[group] = 0
                closed_this_month[group] = 0
                closed_last_month[group] = 0
                missing_fields[group] = 0
            fte_planned_this_month[group] = fte_planned_this_month[group] + int(item['expected_effort_for_month'])
            fte_actual_this_month[group] = fte_actual_this_month[group] + int(item['total_allocation_for_month'])
            if item['starts_this_month']:
                started_this_month[group] = started_this_month[group] + 1
            if item['ends_this_month']:
                ended_this_month[group] = ended_this_month[group] + 1
            if item['closed_this_month']:
                closed_this_month[group] = closed_this_month[group] + 1
            if item['missing_fields']:
                missing_fields[group] = missing_fields[group] + 1
        
        for item in last_month_data:
            try:
                group = item[group_field]
            except:
                group = ""
            fte_planned_last_month[group] = fte_planned_last_month[group] + int(item['expected_effort_for_month'])
            fte_actual_last_month[group] = fte_actual_last_month[group] + int(item['total_allocation_for_month'])
            if item['starts_this_month']:
                started_last_month[group] = started_last_month[group] + 1
            if item['ends_this_month']:
                ended_last_month[group] = ended_last_month[group] + 1
            if item['closed_this_month']:
                closed_last_month[group] = closed_last_month[group] + 1
                
        results['missing_fields'] = missing_fields
        results['fte_planned_this_month'] = fte_planned_this_month
        results['fte_planned_last_month'] = fte_planned_last_month
        results['fte_actual_this_month'] = fte_actual_this_month
        results['fte_actual_last_month'] = fte_actual_last_month
        results['started_this_month'] = started_this_month
        results['started_last_month'] = started_last_month
        results['ended_this_month'] = ended_this_month
        results['ended_last_month'] = ended_last_month
        results['closed_this_month'] = closed_this_month
        results['closed_last_month'] = closed_last_month
        return results
    
    @CacheStore(u"get_dq_summary", 10, vary_by=[1])
    def get_dq_summary(self, month):
        active_packages = PackageFilters(g.packages).active().add_allocation_fields()
        results = {}
        
        """
        Some of the figures we can calculate directly in rethinkdb: 
        """
        results['total_active'] = copy.copy(active_packages) \
                    .count_by_group('areaOfWork') \
                    .execute() \
                    .reformat_counts() \
                    .val()
        
        results['unallocated'] = copy.copy(active_packages) \
                    .filter_by_field('count_of_allocated_resources',0) \
                    .count_by_group('areaOfWork') \
                    .execute().reformat_counts().val()
        
        results['no_activity'] = copy.copy(active_packages) \
                    .greater_than('count_of_allocated_resources',0) \
                    .filter_by_field('total_allocation',0) \
                    .count_by_group('areaOfWork').execute().reformat_counts().val()
        
        results['no_programme'] = copy.copy(active_packages) \
                    .filter_by_field_not_exists_or_blank('programme') \
                    .count_by_group('areaOfWork') \
                    .execute().reformat_counts().val()
        
        results['no_activity_this_month'] = PackageFilters(g.packages).active() \
                    .add_total_allocation_for_month(month) \
                    .filter_by_field('total_allocation_for_month',0) \
                    .count_by_group('areaOfWork').execute().reformat_counts().val()
        
        """
        Others we have to do in python because of the complex date manipulations
        required which are not currently supported in rethinkdb
        """
        data = copy.copy(active_packages).execute().add_expected_effort_and_date_checks().val()
        
        active_past_end_date = {}
        active_before_start_date = {}
        over_allocated = {}
        under_allocated = {}
        over_allocated_this_month = {}
        under_allocated_this_month = {}
        missing_fields = {}
        
        for item in data:
            areaOfWork = 'Unspecified'
            if 'areaOfWork' in item:
                areaOfWork = item['areaOfWork']
            if areaOfWork not in active_past_end_date:
                active_past_end_date[areaOfWork] = 0
                active_before_start_date[areaOfWork] = 0
                over_allocated[areaOfWork] = 0
                under_allocated[areaOfWork] = 0
                missing_fields[areaOfWork] = 0
                over_allocated_this_month[areaOfWork] = 0
                under_allocated_this_month[areaOfWork] = 0
                
            if item['after_end_date'] == True:
                active_past_end_date[areaOfWork] = active_past_end_date[areaOfWork] + 1
            if item['before_start_date'] == True:
                active_before_start_date[areaOfWork] = active_before_start_date[areaOfWork] + 1
            if item['over_allocated'] == True:
                over_allocated[areaOfWork] = over_allocated[areaOfWork] + 1
            if item['under_allocated'] == True:
                under_allocated[areaOfWork] = under_allocated[areaOfWork] + 1
            if item['missing_fields'] == True:
                missing_fields[areaOfWork] = missing_fields[areaOfWork] + 1
        
        results['active_past_end_date'] = active_past_end_date
        results['active_before_start_date'] = active_before_start_date
        results['over_allocated'] = over_allocated
        results['under_allocated'] = under_allocated
        results['missing_fields'] = missing_fields
        
        
        """
        Figures for this month
        """
        month_data = copy.copy(active_packages).execute().add_expected_effort_for_month(month).val()
        for item in month_data:
            if item['over_allocated'] == True:
                over_allocated_this_month[areaOfWork] = over_allocated_this_month[areaOfWork] + 1
            if item['under_allocated'] == True:
                under_allocated_this_month[areaOfWork] = under_allocated_this_month[areaOfWork] + 1
        
        results['over_allocated_this_month'] = over_allocated_this_month
        results['under_allocated_this_month'] = under_allocated_this_month

        return results
