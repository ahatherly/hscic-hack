import rethinkdb as r
from app.utils import *
from app.htmldiff import render_html_diff
import functools, inspect, datetime, copy
from app.cachestore import CacheStore
from flask_login import current_user

class DataStore:
    """
    Generic class for accessing rethinkdb (inspired by python datastore)
    See specs for examples of use
    """

    def __init__(self, rdb_conn, table):
        self.database = rdb_conn
        self.table = table
        self.audit_table = table + '_audit'
        
        self.__create_datastore_table()
        

    def get(self, key, mapper=None):
        """
        Retrieves the model instance (or instances) 
        for the given `key`. 
        The keys must represent entities of the model's kind. 
        If a provided key is not of the correct kind, a KindError 
        exception is raised.
        if a `mapper` method is passed in, the object Attempts
        to return as mapped object
        """
        item = r.table(self.table).get(key).run(self.database)
        if mapper is not None:
            return mapper(item)
        else:
            return item
        
    def delete(self, key):
        """
        Removes the object named by `key`.
        """
        attributes = r.table(self.table).get(key) \
            .delete(return_changes=True).run(self.database)
        
        #print attributes
        self.__audit_delete(attributes["changes"][0]["old_val"], self.__timestamp())

    def post(self, value):
        """
        Stores the object `value`

        Returns generated_key
        """
        value['audit'] = dict(version=1, 
                              inserted=self.__timestamp())
        
        attributes = r.table(self.table) \
            .insert(value, conflict="replace") \
            .run(self.database)
        
        if 'generated_keys' in attributes:
            return attributes['generated_keys'][0]
        else:
            return None
    
    def history(self, key, limit=10):
        """
        Returns history for the object defined by `key`,
        `limit` specifies the number of history records
        """
        self.__create_audit_table()
        
        query = r.table(self.audit_table) \
           .filter( lambda au: au["originalId"]==key) \
           .order_by(r.desc("updated")) \
           .limit(limit)
        
        result = query.run(self.database)
        #print result
        
        for item in result:
            for changeEntry in item['changelog']:
                fieldName = changeEntry['fieldName']
                valFrom = changeEntry['fromValue']
                valTo = changeEntry['toValue']
                if isinstance(valFrom, int):
                    valFrom = str(valFrom)
                if isinstance(valTo, int):
                    valTo = str(valTo)
                #print '** FROM **'
                #print valFrom
                #print '** TO **'
                #print valTo
                diff = ''
                if (fieldName != 'Assigned'):
                    try:
                        diff = render_html_diff(valFrom, valTo)
                    except:
                        diff = 'Unable to show differences - previous value of this field was:<br/>'+valFrom
                else:
                    formattedFrom = formatAllocationsAsHTML(valFrom)
                    formattedTo = formatAllocationsAsHTML(valTo)
                    diff = render_html_diff(formattedFrom, formattedTo)
                #print '** DIFF **'
                #print diff                    
                changeEntry['diffValue'] = diff
        
        return result
    
    
    def __create_datastore_table(self):
        """
        Utility function to create datastore table
        if none exists, also adds `audit` field
        to current table
        """
        table_list = r.db(self.database.db).table_list().run(self.database) 
        
        if not self.table in table_list:
            r.db(self.database.db).table_create(self.table) \
                .run(self.database)
    
    def __create_audit_table(self):
        """
        Utility function to create audit table
        if none exists, also adds `audit` field
        to current table
        """
        table_list = r.db(self.database.db).table_list().run(self.database) 
        
        if not self.audit_table in table_list:
            r.db(self.database.db).table_create(self.audit_table) \
                .run(self.database)
        
            self.all().filter(r.row.without("audit")) \
                .update( {'audit': dict( version=1 )}).run(self.database)
    
    def __timestamp(self):
        return timestamp()
    
    def rethinkTimstamp(self):
        #return r.now().run(self.database)
        return r.now()
    
    def __audit_delete(self, old_value, updated):
        self.__create_audit_table()
        
        audit_record = dict(updated=updated,
                           version=old_value['audit']["version"],
                           originalId=old_value["id"],
                           changedBy=current_user.name,
                           changelog=[])
        
        audit_record['changelog'].append(dict(fieldName='document', 
                              fromValue=old_value,
                              toValue='deleted' ))
        
        r.table(self.audit_table) \
            .insert(audit_record) \
            .run(self.database)
    
    def __audit_update(self, old_value, new_value, updated):
        self.__create_audit_table()
        
        # Fix for updates to records with no audit record
        if 'audit' not in old_value:
            old_value['audit'] = dict(version=0)
        
        audit_record = dict(updated=updated,
                       version=old_value['audit']["version"],
                       originalId=old_value["id"],
                       changedBy=current_user.name,
                       changelog=[])
        
        
        for k, v in new_value.items():
            
            if k == 'audit':
                continue
            
            previous_value = ''
            if old_value.has_key(k):
                previous_value = old_value[k]
            
            if (previous_value == '' and v != '') or previous_value != v:
                change = dict(fieldName=k, 
                              fromValue=previous_value,
                              toValue=v)
                audit_record['changelog'].append(change)
                
        
        r.table(self.audit_table) \
            .insert(audit_record) \
            .run(self.database)
    
    def put(self, key, value):
        """
        Replaces the object `value` named by `key`.

        returns number of updated documents, and the attributes
        """
        updated = self.__timestamp()
        
        new_value = copy.deepcopy(value)
        
        value['audit'] = dict(updated=updated,
                              version=r.row["audit"]['version'].add(1))        
        
        # Do the update - this returns the old value for inclusion in the audit table
        attributes = r.table(self.table) \
            .get(key) \
            .update(value, durability="hard", return_changes=True, non_atomic=False) \
            .run(self.database)
        
        self.__audit_update(attributes["changes"][0]["old_val"], new_value, updated)
        
        return attributes["replaced"]
    
    def put_noaudit(self, key, value):
        """
        Replaces the object `value` named by `key`.

        returns number of updated documents, and the attributes
        """
        attributes = r.table(self.table) \
            .get(key) \
            .update(value, durability="hard", return_changes=True, non_atomic=False) \
            .run(self.database)
        
        return attributes["replaced"]
    
    def all(self):
        """
        returns filter set for use by caller 
        """
        return r.table(self.table)
    
    def query(self, query):
        """
        Returns an iterable of objects matching criteria expressed in `query`

        Use the `all()` method to build query filter 
        """
        return query.run(self.database)

    def query_single(self, query, mapper=None):
        """Executed in 54ms. 40 rows 
        Returns a single object from rethinkdb
        """
        cursor = self.query(query)
        val = self.__decursor_single(cursor)
        #print val
        if mapper:
            return mapper(val)
        else:
            return val

    def contains(self, key):
        """
        Returns whether the object named by `key` exists.
        """
        return self.get(key) is not None

    @classmethod
    def __decursor_single(cls, cursor):
        """
        Attempts to return the first item from a rethinkdb cursor
        """
        rethink_list = list(cursor)
        if len(rethink_list) > 0:
            return rethink_list[0]
        else:
            return None
