import rethinkdb as r
from app import app
from app.datastores.datastore import DataStore


class PortfolioStore(DataStore):
    
    def __init__(self, rdb_conn, portfolioTable="portfolio"):
        #DataStore.__init__(self, rdb_conn, portfolioTable)
        self.database = rdb_conn
        self.table = portfolioTable

    def clear_existing_programmes(self):
        r.table(self.table).delete().run(self.database)
    
    def create_table_if_not_exists(self):
        exists = r.table_list().contains(self.table).run(self.database)
        if exists == False:
            r.table_create(self.table).run(self.database)
        
    def add_programme(self, code, name, description, status):
        new_programme = {}
        new_programme['code'] = code
        new_programme['name'] = name
        new_programme['description'] = description
        new_programme['status'] = status
        new_programme['created'] = self.rethinkTimstamp()
        guid = self.post(new_programme)
        return guid
    
    def list(self):
        query = self.all()
        return self.query(query)
    
    def get_list_values(self):
        data = self.query(self.all())
        result = []
        for val in data:
            item = {}
            item['value']=val['code']
            item['text']=val['code']+' : '+val['name']+' ('+val['status'].title()+')'
            result.append(item)
            """
            Return the actual list of values
            """
        return result