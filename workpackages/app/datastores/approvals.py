from app.datastores.datastore import DataStore
from app.models import User

class ApprovalsStore(DataStore):
    """
    Implementation of approvals store
    """
    def __init__(self, rdb_conn):
        DataStore.__init__(self, rdb_conn, "approvals")
    
    def add_pending(self, user):
        pending_user = User.map_from(user)
        #print "PENDING USER TO ADD"
        #print pending_user
        approval = self.post(pending_user)
    
    def get_unapproved(self):
        return self.query(self.all())
    
    def get_unapproved_count(self):
        return self.query(self.all().count())

    def approve(self, approval_id):
        pass
