"""
Pre-defined filters and mappings for work packages for reporting, DQ, etc
Allows for the use of the builder pattern to (hopefully) simplify code and promote
re-use - for example to get the total of all allocations in active QIPP work packages:

PackageFilters(g.packages).active().portfolio("QIPP, Interop, xGov").total_allocations().execute().result()

"""
import rethinkdb as r
import math
from app.utils import *
from app import app
import datetime

class PackageFilters:
    
    def __init__(self, p):
        self.pStore = p
        self.filteredQuery = self.pStore.all().has_fields('status').filter(r.row['status'].ne("Deleted"))

    def execute(self):
        self.result = self.pStore.query(self.filteredQuery)
        if type(self.result) == r.net.Cursor:
            self.result = list(self.result)
        if type(self.result) == r.net.DefaultCursor:
            self.result = list(self.result)
        return self
    
    def execute_single(self):
        self.result = self.pStore.query_single(self.filteredQuery)
        if type(self.result) == r.net.Cursor:
            self.result = list(self.result)
        return self
    
    def val(self):
        return self.result
    
    """
    Basic filters
    """
    def by_id(self, wp_id):
        self.filteredQuery = self.filteredQuery.filter({'id':wp_id})
        return self
    def active(self):
        self.filteredQuery = self.filteredQuery.filter({'status':'Active'})
        return self
    
    def closed(self):
        self.filteredQuery = self.filteredQuery.filter({'status':'Closed'})
        return self
    
    def defining(self):
        self.filteredQuery = self.filteredQuery.filter({'status':'Defining'})
        return self
    
    def on_hold(self):
        self.filteredQuery = self.filteredQuery.filter({'status':'On Hold'})
        return self
    
    def portfolio(self, portfolio):
        self.filteredQuery = self.filteredQuery.has_fields('portfolio').filter({'portfolio':portfolio})
        return self
    
    def area_of_work(self, aow):
        self.filteredQuery = self.filteredQuery.has_fields('areaOfWork').filter({'areaOfWork':aow})
        return self
    
    def filter_by_field(self, field, value):
        self.filteredQuery = self.filteredQuery.filter({field: value})
        return self
    
    def filter_by_field_not_exists_or_blank(self, field):
        self.filteredQuery = self.filteredQuery.filter({field:''}, default=True)
        return self

    def filter(self, f):
        self.filteredQuery = self.filteredQuery.filter(f)
        return self
    
    def greater_than(self, field, value):
        self.filteredQuery = self.filteredQuery.filter(lambda wp: wp[field] > value)
        return self
    
    def less_than(self, field, value):
        self.filteredQuery = self.filteredQuery.filter(lambda wp: wp[field] < value)
        return self
    
    def active_or_closed_in_months(self, monthyear1, monthyear2):
        """
        r.db('workpackages').table('packages').filter(
            function(doc){
              return ( 
                doc('status').eq('Active').or(
                ((doc('closedDate').match("../04/2014|../05/2014")).and(doc('status').eq('Closed')))
                )
              )
            }
          )
        })
        """
        def included(wp):
            return \
                wp['status'].eq('Active') | \
                    ( wp['closedDate'].match("../"+monthyear1+"|../"+monthyear2) & wp['status'].eq('Closed') )
        
        self.filteredQuery = self.filteredQuery.filter(included)
        return self

    """
    Add calculated fields to documents
    """
    def add_allocation_fields(self):
        def map_assignments(wp):
            # Total allocations for the work package
            total=wp['Assigned'].map(map_total_allocations) \
                .reduce(lambda acc, val: acc + val).default(0)
            # Fixed above reduce for new rethinkdb version
            # Number of people allocated to the work package
            count_of_allocated_resources=wp['Assigned'].count()
            return wp.merge(dict(total_allocation=total, \
                                 count_of_allocated_resources=count_of_allocated_resources))
        def map_total_allocations(allocations):
            total = allocations['Allocations'].coerce_to('array') \
                            .map(lambda valList: valList.nth(1)) \
                            .reduce(lambda acc, val: acc + val).default(0)
            # Fixed above reduce for new rethinkdb version
            return total
        self.filteredQuery = self.filteredQuery.map(map_assignments)
        return self
    
    def add_total_allocation_for_month(self, month):
        def get_assignments_for_month(assign):
            return assign['Allocations'][month].default(0)
        def map_assignments(wp):
            # Total allocations for the work package this month
            total=wp['Assigned'].map(get_assignments_for_month) \
                .reduce(lambda acc, val: acc + val).default(0)
            # Fixed above reduce for new rethinkdb version
            return wp.merge(dict(total_allocation_for_month=total))
        self.filteredQuery = self.filteredQuery.map(map_assignments)
        return self
    
    def add_total_allocation_for_month_with_named_output(self, month, output_key_name):
        def get_assignments_for_month(assign):
            return assign['Allocations'][month].default(0)
        def map_assignments(wp):
            # Total allocations for the work package this month
            total=wp['Assigned'].map(get_assignments_for_month) \
                .reduce(lambda acc, val: acc + val).default(0)
            # Fixed above reduce for new rethinkdb version
            result = {output_key_name:total}
            return wp.merge(result)
        self.filteredQuery = self.filteredQuery.map(map_assignments)
        return self
    
    """
    (Rough) Javascript equivalent:
    r.db('workpackages').table('packages').filter({'id':'42a5f1c4-b7cd-4e91-af38-306a7c82a3b6'})
      .map(
        function(wp) { return wp('Assigned').filter({'Name':'Adam Hatherly'}).map(
            function(allocations) { return allocations('Allocations').coerceTo('array') 
                                              .map(function(i) { return i.nth(1) })
                                              .reduce(function(acc, val) { return acc.add(val) }).default(0)
            })
        })
    """
    def add_total_allocation_for_person(self, person):
        def map_total_allocations(allocations):
            total = allocations['Allocations'].coerce_to('array') \
                        .map(lambda valList: valList.nth(1)) \
                        .reduce(lambda acc, val: acc + val).default(0)
            return total
        def map_assignments(wp):
            # Total allocations for the work package this month
            total=wp['Assigned'].filter({'Name':person}).map(map_total_allocations) \
                .reduce(lambda acc, val: acc + val).default(0)
            # Fixed above reduce for new rethinkdb version
            return wp.merge(dict(total_allocation_for_person=total))
            return dict(total_allocation_for_person=total)
        self.filteredQuery = self.filteredQuery.map(map_assignments)
        return self
    
    """
    Ordering
    """    
    def order_by(self, field):
        self.filteredQuery = self.filteredQuery.order_by(field)
        return self
    
    def order_by_multiple(self, *fields):
        self.filteredQuery = self.filteredQuery.order_by(*fields)
        return self
    
    """
    Transformations
    """
    def total_allocations(self):
        def map_assignments(assigned):
            return assigned['Assigned'].map(map_total_allocations) \
                .reduce(lambda acc, val: acc + val['total']).default(0)
            # Fixed above reduce for new rethinkdb version
        
        def map_total_allocations(allocations):
            total = allocations['Allocations'].coerce_to('array') \
                            .map(lambda valList: valList.nth(1)) \
                            .reduce(lambda acc, val: acc + val).default(0)
            # Fixed above reduce for new rethinkdb version
            return dict(total=total)
        
        self.filteredQuery = self.filteredQuery.map(map_assignments) \
                .reduce(lambda acc, val: acc + val).default(0)
        # Fixed above reduce for new rethinkdb version
        return self
    
    def total_allocations_specific_month(self, month):
        
        def getAssignmentForMonth(assigned):
            return assigned['Allocations'][month].default(0)
        
        def map_assignments(wp):
            return wp['Assigned'].map(getAssignmentForMonth) \
                .reduce(lambda acc, val: acc + val).default(0)
            # Fixed above reduce for new rethinkdb version
        
        self.filteredQuery = self.filteredQuery.map(map_assignments) \
                .reduce(lambda acc, val: acc + val).default(0)
        # Fixed above reduce for new rethinkdb version
        return self
    
    """
    Javascript version:
    r.db('workpackages').table('packages').filter({'id':'0be0d2ca-9cf1-42f0-ab21-6bc46389df6a'})
        .map(function(workpackage) { return workpackage.merge(
                                            {latest_update : workpackage('StatusUpdates')
                                                                .default({'date':'None','update':'None','author':'None','report_tags':[]})
                                                                .orderBy(r.desc('date')).nth(0)
                                            }
                                        ) })
    """
    def add_latest_status_update(self):

        def get_latest_update(workpackage):
            default_update = {'date':None,'update':None,'author':None,'report_tags':[]}
            latest_update = workpackage['StatusUpdates'].default([default_update]).order_by(r.desc('date')).nth(0)
            return workpackage.merge(dict(latest_update=latest_update))
        
        self.filteredQuery = self.filteredQuery.map(get_latest_update)
        return self
    
    """
    Javascript version:
    r.db('workpackages').table('packages').filter({'id':'0be0d2ca-9cf1-42f0-ab21-6bc46389df6a'}).hasFields('StatusUpdates')
        .map(function(workpackage) { return workpackage.merge(
                                            {month_update : workpackage('StatusUpdates')
                                                               .filter(function(u) { return u('report_tags').contains('jun14') })
                                            }
                                        ) })
    """
    def add_status_update_for_month(self, month_name):
        
        def contains_month(update):
            return update['report_tags'].contains(month_name)
        
        def get_month_update(workpackage):
            default_update = {'date':None,'update':None,'author':None,'report_tags':[]}
            month_update = workpackage['StatusUpdates'].default([default_update]).filter(contains_month).nth(0).default(default_update)
            return workpackage.merge(dict(month_update=month_update))
        
        self.filteredQuery = self.filteredQuery.map(get_month_update)
        return self
    
    """
    Outputs
    """
    def distinct(self):
        self.filteredQuery = self.filteredQuery.distinct()
        return self
    
    def pluck(self, field_list):
        self.filteredQuery = self.filteredQuery.pluck(*field_list)
        return self
    
    def without(self, field_list):
        self.filteredQuery = self.filteredQuery.without(*field_list)
        return self
    
    def count(self):
        self.filteredQuery = self.filteredQuery.count()
        return self

    def count_by_group(self, *group_field):
        """
        Before reformat:
        [{u'reduction': 2, u'group': u''},
         {u'reduction': 31, u'group': u'Cross-programme Services and NHS Facing'},
         ...
         {u'reduction': 37, u'group': u'Patient Facing Systems'}]
        """
        def reformat(result):
            return dict(group=result['group'], value=result['reduction'])
        # Above line altered for new rethinkdb version - group results come back differently now
        
        self.filteredQuery = self.filteredQuery \
                            .has_fields(*group_field) \
                            .group(*group_field).count().ungroup() \
                            .map(reformat)
        # Fixed above group statement for new rethinkdb version
        return self
    
    def sum_for_group(self, group_field, sum_field):
        self.filteredQuery = self.filteredQuery \
                            .group(group_field).sum(sum_field).ungroup()
        # Fixed above group statement for new rethinkdb version
        return self
    
    def sum_for_group_multiple(self, sum_field, *group_fields):
        self.filteredQuery = self.filteredQuery \
                            .group(*group_fields).sum(sum_field).ungroup()
        # Fixed above group statement for new rethinkdb version
        return self
    
    def sum_for_group_first_match_only(self, group_field, sum_field):
        self.filteredQuery = self.filteredQuery \
                            .group(group_field).sum(sum_field).ungroup() \
                            .pluck('reduction')[group_field]['reduction']
        # Fixed above group statement for new rethinkdb version
        return self
    
    """
    Transformations on output documents (python only - not rethink cursors)
    Methods take an array of work package objects and return array of modified objects
    THESE CAN ONLY BE RUN AFTER CALLING execute()
    """
    
    def reformat_counts(self):
        output = {}
        for item in self.result:
            output[item['group']] = item['value']
        self.result = output
        return self
    
    def filter_dict(self, field, value):
        output = []
        for item in self.result:
            if field in item:
                if item[field] == value:
                    output.append(item)
        self.result = output
        return self
    
    def greater_than_dict(self, field, value):
        output = []
        for item in self.result:
            if field in item:
                try:
                    if int(item[field]) > value:
                        output.append(item)
                except:
                    pass
        self.result = output
        return self
    
    def less_than_dict(self, field, value):
        output = []
        for item in self.result:
            if field in item:
                try:
                    if int(item[field]) < value:
                        output.append(item)
                except:
                    pass
        self.result = output
        return self
    
    def add_expected_effort_and_date_checks(self):
        under = app.config['UNDER_ALLOCATION_TOLERANCE_PERCENT']
        under_days = app.config['UNDER_ALLOCATION_TOLERANCE_DAYS']
        over = app.config['OVER_ALLOCATION_TOLERANCE_PERCENT']
        over_days = app.config['OVER_ALLOCATION_TOLERANCE_DAYS']
        now = datetime.datetime.now()
        
        for item in self.result:
            try:
                start_date = parse_date(item['forecastStartDate'])
                end_date = parse_date(item['forecastEndDate'])
            except:
                start_date = None
                end_date = None
            item['missing_fields'] = False
            try:
                percentageFTE = int(item['percentageFteEffort'])
            except:
                percentageFTE = 0
                item['missing_fields'] = True
            
            try:
                total_allocation = int(item['total_allocation'])
            except:
                total_allocation = None
                
            # Only try to calculate if we have valid start and end dates
            if start_date != None and end_date != None:
                working_days = business_days(start_date, end_date)
                expected_effort = int(math.ceil(working_days * (float(percentageFTE)/100)))
                item['expected_effort'] = expected_effort
                item['working_days'] = working_days
                
                if now < start_date:
                    item['before_start_date'] = True
                else:
                    item['before_start_date'] = False
                if now > end_date:
                    item['after_end_date'] = True
                else:
                    item['after_end_date'] = False
                
                # Over/Under allocated
                max_days_expected = max(expected_effort * over, expected_effort + over_days)
                min_days_expected = min(expected_effort * under, expected_effort - under_days)
                
                if total_allocation == None: 
                    item['over_allocated'] = False
                    item['under_allocated'] = False
                elif total_allocation > max_days_expected:
                    item['over_allocated'] = True
                    item['under_allocated'] = False
                elif total_allocation < min_days_expected:
                    item['over_allocated'] = False
                    item['under_allocated'] = True
                else:
                    item['over_allocated'] = False
                    item['under_allocated'] = False
            else:
                item['expected_effort'] = 0
                item['working_days'] = 0
                item['over_allocated'] = False
                item['under_allocated'] = False
                item['before_start_date'] = False
                item['after_end_date'] = False
                item['missing_fields'] = True
            
        return self
    
    def add_expected_effort_for_month(self, month):
        under = app.config['UNDER_ALLOCATION_TOLERANCE_PERCENT']
        under_days = app.config['UNDER_ALLOCATION_TOLERANCE_DAYS']
        over = app.config['OVER_ALLOCATION_TOLERANCE_PERCENT']
        over_days = app.config['OVER_ALLOCATION_TOLERANCE_DAYS']
        
        for item in self.result:
            try:
                start_date = parse_date(item['forecastStartDate'])
                end_date = parse_date(item['forecastEndDate'])
            except:
                start_date = None
                end_date = None
            item['missing_fields'] = False
            closed_date = None
            if 'closedDate' in item:
                closed_date = parse_date(item['closedDate'])
            item['starts_this_month'] = False
            item['ends_this_month'] = False
            item['closed_this_month'] = False
            try:
                percentageFTE = int(item['percentageFteEffort'])
            except:
                percentageFTE = 0
                item['missing_fields'] = True
            
            try:
                total_allocation = int(item['total_allocation_for_month'])
            except:
                total_allocation = None
            
            first_of_month = first_day_of_month(month)
            last_of_month = last_day_of_month(month)
            
            # Checks for closed date
            if closed_date != None:
                if closed_date > first_of_month and closed_date < last_of_month:
                    item['closed_this_month'] = True
            
            # Only try to calculate if we have valid start and end dates
            if start_date != None and end_date != None:
                days = 0
                if start_date > first_of_month and end_date < last_of_month:
                    # Whole work package within specified month
                    days = business_days(start_date, end_date)
                    item['starts_this_month'] = True
                    item['ends_this_month'] = True
                elif start_date <= first_of_month and end_date < last_of_month:
                    # Starts before specified month, but ends in specified month
                    days = business_days(first_of_month, end_date)
                    item['ends_this_month'] = True
                elif start_date > first_of_month and end_date >= last_of_month:
                    # Starts in the specified month and ends after specified month
                    days = business_days(start_date, last_of_month)
                    item['starts_this_month'] = True
                elif start_date <= first_of_month and end_date >= last_of_month:
                    # Starts before specified month, and ends after specified month
                    days = business_days(first_of_month, last_of_month)
                
                expected_effort = int(math.ceil(days * (float(percentageFTE)/100)))
                item['expected_effort_for_month'] = expected_effort
                item['working_days_within_month'] = days
                
                # Over/Under allocated
                max_days_expected = max(expected_effort * over, expected_effort + over_days)
                min_days_expected = min(expected_effort * under, expected_effort - under_days)
                
                if total_allocation == None: 
                    item['over_allocated'] = False
                    item['under_allocated'] = False
                elif total_allocation > max_days_expected:
                    item['over_allocated'] = True
                    item['under_allocated'] = False
                elif total_allocation < min_days_expected:
                    item['over_allocated'] = False
                    item['under_allocated'] = True
                else:
                    item['over_allocated'] = False
                    item['under_allocated'] = False
                
            else:
                item['expected_effort_for_month'] = 0
                item['working_days_within_month'] = 0
                item['over_allocated'] = False
                item['under_allocated'] = False
                item['missing_fields'] = True
        
        return self
