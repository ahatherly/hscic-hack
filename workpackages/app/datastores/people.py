import rethinkdb as r
from app.models import User
from app.utils import *
from app.datastores.datastore import DataStore

from app.cachestore import CacheStore

class PeopleStore(DataStore):
    """
    Implementation of store of people
    """

    def __init__(self, rdb_conn):
        DataStore.__init__(self, rdb_conn, "people")

    def one_by_name(self, name):
        """
        Returns user by name
        """
        query = self.all().filter( { 'name': name })
        return self.query_single(query, User.map_to)
    
    def delete_person(self, person_id):
        return self.delete(person_id)

    def one_by_email(self, email):
        """
        Returns user by email address
        """
        query = self.all().filter( { 'email': email })
        return self.query_single(query)

    def get_all(self):
        """
        Returns all users
        """
        query = self.all()
        return self.query(query)
    
    def update_person(self, personid, fieldname, newvalue):
        """
        Updates person
        """
        self.put(personid, {fieldname:newvalue})
        return 'Updated'
    
    @CacheStore(u"people_as_list_values", 1)
    def get_all_names(self):
        people = self.query(self.all() \
                            .order_by('name'))
        return [person["name"] for person in people]

    def get_user_by_name(self, name):
        query = self.all() \
            .filter( { 'name': name }) \
            .pluck(*FIELD_LIST['person'])
        return self.query(query)

    def user_logged_in(self, user_id):
        self.query( self.all() \
            .update ({ 'lastlogin': timestamp() }))

    def get_user_by_auth(self, authprovider, authid):
        query = self.all() \
            .filter({ 
                'authprovider': authprovider,
                'authid': authid
            })
        return self.query_single(query, User.map_to)

    @CacheStore(u"person session user", 1, vary_by=[1])
    def get_session_user(self, user_id):
        return self.get(user_id, User.map_to)
    
    @CacheStore(u"person_get_list_values", 1, vary_by=[1])
    def get_list_values(self, field_name):
        
        list_values = self.query(self.all().filter(r.row['status'].eq("Active")).pluck(field_name).distinct().has_fields(field_name))

        """
        Check if this is empty and return an empty list to avoid our cache blowing up
        """
        if len(list_values) == 1:
            if (list_values[0] == {}):
                return [{u'value': u'', u'text': u''}];
        
        """
        Return the actual list of values
        """
        return [{u'value': list_value[field_name], 
                 u'text': list_value[field_name]} 
                for list_value in list_values]

