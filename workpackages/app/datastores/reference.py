from app.datastores.datastore import DataStore

class ReferenceStore(DataStore):
    """
    Implementation of approvals store
    """
    def __init__(self, rdb_conn):
        DataStore.__init__(self, rdb_conn, "reference")
    
    def get_values(self, field_name):
        q = self.all().filter( { 'field_name': field_name })
        return self.query_single(q)

    def get_all(self):
        return self.query(self.all())
        