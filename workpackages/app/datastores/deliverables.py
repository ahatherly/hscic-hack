from app.datastores.datastore import DataStore

class DeliverablesStore(DataStore):
    """
    Implementation of deliverables store
    """
    def __init__(self, rdb_conn):
        DataStore.__init__(self, rdb_conn, "deliverables")
    
    def get_values(self, field_name):
        q = self.all().filter( { 'serviceType': field_name })
        return self.query_single(q)

    def get_all(self):
        return self.query(self.all())

    def update_field(self, id, fieldname, newvalue):
        """
        Updates workpackage
        """
        self.put(id, {fieldname:newvalue})
        return 'Updated'
