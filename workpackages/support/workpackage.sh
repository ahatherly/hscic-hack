#!/bin/sh
cd /var/webapp/env/bin
. activate
if [ "$#" -gt 1 ]; then
  echo "USAGE : workpackage start / stop / restart" && exit 1
fi

if [ "$#" -eq 0 ]; then
  echo "USAGE : workpackage start / stop / restart" && exit 1
fi

if [ "$1" = "start" ]; then
  #  Start Workpackage
 echo "Starting Workpackage services..."
 echo "..."
 echo "Rethinkdb Starting..."
 echo "..."
 service rethinkdb start
 echo "..."
 echo "Nginx Starting..."
 echo "..."
 service nginx start
 echo "..."
 echo "GUnicorn Starting..."
 echo "..."
 cd /var/webapp/hscic-hack/workpackages
 gunicorn app:app --bind unix:/tmp/gunicorn_flask.sock -w 4 -D
 echo "..."
 echo "Workpackage started"
 exit 1
fi
if [ "$1" = "stop" ]; then
  # Stop Workpackage
  echo "Stopping Workpackage services..."
  pkill gunicorn
  service rethinkdb stop
  service nginx stop
  exit 1
fi
if [ "$1" = "restart" ]; then
  # restart workpackage
  echo "Restarting Workpackage services..."
  cd /var/webapp/hscic-hack/workpackages
  pkill gunicorn
  service rethinkdb restart
  service nginx restart
  gunicorn app:app --bind unix:/tmp/gunicorn_flask.sock -w 4 -D
  exit 1
fi

exit 0
