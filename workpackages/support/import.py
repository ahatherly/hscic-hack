# -*- coding: utf-8 -*-
# 31/05/2013 HSCIC Hackday

# import routine
import sys, json, csv
from collections import defaultdict
import rethinkdb as r

r.connect('localhost', 28015).repl()

if sys.argv[1]=="createdb":
  r.db_create('workpackages').run()
  r.db('workpackages').table_create('packages').run()
  r.db('workpackages').table_create('people').run()
  r.db('workpackages').table_create('reference').run()

elif sys.argv[1]=="createref":
  r.db('workpackages').table_create('reference').run()

elif sys.argv[1]=="dropref":
  r.db('workpackages').table_drop('reference').run()

elif sys.argv[1]=="dropdb":
  r.db_drop('workpackages').run()

elif sys.argv[1]=="removeu":
  r.db('workpackages').table_drop('people').run()
  r.db('workpackages').table_create('people').run()
  print "Users removed"

elif sys.argv[1]=="importr":
  reference = r.db('workpackages').table('reference')

  data = open("reference.csv")
  reader = csv.DictReader(data, delimiter=",", quotechar='"')

  field_list = {}

  for row in reader:
     field_name = row['field_name'].decode('utf8','ignore')
     field_value = row['field_value'].decode('utf8','ignore')
     if field_name in field_list:
         field_list[field_name].append(field_value)
     else:
         field_list[field_name] = []
         field_list[field_name].append(field_value)
  for k, v in field_list.items():
      item = {}
      item['field_name'] = k
      item['field_values'] = v
      
      # Check if record already exists, and if so replace it
      oldid = None
      for r in reference.get_all(k, index='field_name').run():
          oldid = r['id']
      if oldid != None:
          item['id'] = oldid
      
      print reference.insert(item, dict(upsert=True)).run()
  reference.index_create('field_name').run()
  print "Done"

elif sys.argv[1]=="importu":
  packages = r.db('workpackages').table('people')

  data = open("users.csv")
  reader = csv.DictReader(data, delimiter=",", quotechar='"')

  user_data = {}

  for row in reader:
     useritem = {}
     for k, v in row.items():
        useritem[k]= v.decode('utf8','ignore')
     print packages.insert(useritem).run()
  packages.index_create('name').run()

elif sys.argv[1]=="importw":
  packages = r.db('workpackages').table('packages')

  data = open("workpackages.csv")
  reader = csv.DictReader(data, delimiter=",", quotechar='"')

  cleansing = {
    "estimate % of effort for an FTE?": "percentageFteEffort" ,
    "Work Package name":  "name" ,
    "Work Package Lead":  "leadPerson" ,
    "Status Update":  "statusUpdate" ,
    "Status":  "status" ,
    "Reference":  "reference" ,
    "Portfolio Owner":  "portfolioOwner" ,
    "Portfolio":  "portfolio" ,
    "Path":  "path" ,
    "Modified By":  "modifiedBy" ,
    "Modified":  "modified" ,
    "Item Type":  "itemType" ,
    "Forecast Start Date":  "forecastStartDate" ,
    "Forecast End Date":  "forecastEndDate" ,
    "Description":  "description" ,
    "Deliverables": "deliverables",
    "Created":  "created",
    "Area of Work":  "areaOfWork",
    "Tech Office Service Type": "serviceType",
    "Level 1 TAD Service Type": "level1Type",
    "Level 2 TAD Service Type": "level2Type"
  }

  

  package_data = {}
  package_workitems = []
  package_allocations = []

  for row in reader:
     workitem = {}
     for k, v in row.items():
        workitem[cleansing[k]]= v.decode('utf8','ignore').strip() # Strip leading and trailing whitespace
     workitem['Assigned']=[]
     print packages.insert(workitem).run()
  packages.index_create('name').run()
  packages.index_create("portfolios", lambda wp: wp['portfolio']).run()
  packages.index_create("portfolios-status", lambda wp: [wp['portfolio'], wp['status']]).run()

elif sys.argv[1]=="removea":
  packages = r.db('workpackages').table('packages')
  for w in packages.run():
    oldid = w['id']
    print packages.get(oldid).update({'Assigned': []}).run()

elif sys.argv[1]=="importa":
  package_allocations = []
  cleanallocation = {
    "Allocated To": "allocated",
    "Reference":  "reference",
    "Work Package name" : "workpackagename",
    "Role": "role",
    "Status": "status",
    "Nov-12": "nov12",
    "Dec-12": "dec12",
    "Jan-13": "jan13",
    "Feb-13": "feb13",
    "Mar-13": "mar13",
    "Apr-13": "apr13",
    "Portfolio Owner": "portfolioowner",
    "Portfolio" : "portfolio",
    "Apr-12": "apr12",
    "Aug-12": "aug12",
    "Created": "created",
    "Created By": "createdby",
    "Dec-11": "dec11",
    "Feb-12": "feb12",
    "Jan-12": "jan12",
    "Jul-12": "jul12",
    "Jun-12": "jun12",
    "Mar-12": "mar12",
    "May-12": "may12",
    "Nov-11": "nov11",
    "Oct-12": "oct12",
    "Sep-12": "sep12",
    "Item Type": "itemtype",
    "Path": "path"
     }
  assignedStrut = {}
  packages = r.db('workpackages').table('packages')
  data = open("workpackages-allocations.csv")
  reader = csv.DictReader(data, delimiter=",", quotechar='"')

  for row in reader:
    workitem_alloc = {}
    assigned = {}
    for k, v in row.items():
 
      workitem_alloc[cleanallocation[k]] = v.decode('utf8', 'ignore').strip()
      # Truncate WP name
      if k == "Work Package name":
        workitem_alloc[cleanallocation[k]] = v[0:120].decode('utf8', 'ignore')

    # Get previous assignment values (if any)
    oldid = "NONE"
    for w in packages.get_all(workitem_alloc['workpackagename'], index='name').run():
      # Check if the reference also matches - there are lots of duplicates in the "name" field!
      if w['reference'] == workitem_alloc['reference']:
        oldassigned = w['Assigned']
        oldid = w['id']

    if oldid != "NONE":
      print "WP Name"
      print workitem_alloc['workpackagename']
      print "WP ID"
      print oldid
      
      print "Old assigned object from DB:"
      print oldassigned

      # Now populate into json
      assigned['Name'] = workitem_alloc['allocated']
      assigned['Status'] = workitem_alloc['status']
      #assigned['Allocations']
      allocations = {}
      allocations['nov11'] = workitem_alloc['nov11'] 
      allocations['dec11'] = workitem_alloc['dec11']
      allocations['jan12'] = workitem_alloc['jan12']
      allocations['feb12'] = workitem_alloc['feb12']
      allocations['mar12'] = workitem_alloc['mar12']
      allocations['apr12'] = workitem_alloc['apr12']
      allocations['may12'] = workitem_alloc['may12']
      allocations['jun12'] = workitem_alloc['jun12']
      allocations['jul12'] = workitem_alloc['jul12']
      allocations['aug12'] = workitem_alloc['aug12']
      allocations['sep12'] = workitem_alloc['sep12']
      allocations['oct12'] = workitem_alloc['oct12']
      allocations['nov12'] = workitem_alloc['nov12']    
      allocations['dec12'] = workitem_alloc['dec12']    
      allocations['jan13'] = workitem_alloc['jan13']    
      allocations['feb13'] = workitem_alloc['feb13']    
      allocations['mar13'] = workitem_alloc['mar13']
      allocations['apr13'] = workitem_alloc['apr13']
     
      assigned['Allocations'] = allocations

      print "New assigned object to add:"
      print assigned


      oldassigned.append(assigned)

      print "New combined assigned object to put in DB:"
      print oldassigned
    
      print packages.get(oldid).update({'Assigned': oldassigned}).run()
    