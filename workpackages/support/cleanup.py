# -*- coding: utf-8 -*-
# 31/05/2013 HSCIC Hackday

# import routine
import sys, json, csv
from collections import defaultdict
import rethinkdb as r
from copy import deepcopy

r.connect('localhost', 28015).repl()

packages = r.db('workpackages').table('packages').run()
table = r.db('workpackages').table('packages')

for package in packages:
    updated = False
    id = package['id']
    
    """
    Convert FTE effort to a number
    """
    if 'percentageFteEffort' in package.keys(): 
        val = package['percentageFteEffort']
        if val == "":
            package['percentageFteEffort'] = 0
            updated = True
        elif type(val) != int:
            if '%' in val:
                val = val.replace('%','')
            package['percentageFteEffort'] = int(val)
            updated = True
    
    """
    Convert allocations to a number, and remove empty entries
    """
    assignments = package['Assigned']
    if (len(assignments)) > 0:
        for index in range(0, len(assignments)):
            allocations = package['Assigned'][index]['Allocations']
            oldallocations = deepcopy(allocations)
            for key in oldallocations:
                val = oldallocations[key]
                if val == "":
                    del package['Assigned'][index]['Allocations'][key]
                    updated = True
                elif type(val) != int:
                    if '%' in val:
                        val = val.replace('%','')
                    if '.00' in val:
                        val = val.replace('.00','')
                    if '.50' in val:
                        val = val.replace('.50','')
                    package['Assigned'][index]['Allocations'][key] = int(val)
                    updated = True
    
    """
    Remove duplicate allocated people
    """
    name_list = []
    indexes_to_delete = []
    assignments = package['Assigned']
    oldassignments = deepcopy(assignments)
    if (len(oldassignments)) > 0:
        for index in range(0, len(oldassignments)):
            assignment = package['Assigned'][index]
            name = assignment['Name']
            if name in name_list:
                indexes_to_delete.append(index)
                print "Found duplicate assignment for " + name + " in work package " + id
            else:
                name_list.append(name)
    for i in reversed(indexes_to_delete):
        del package['Assigned'][i]
        updated = True
    
    if (updated):
        table.get(id).replace(package).run()
print "Done."
