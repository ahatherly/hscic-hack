# -*- coding: utf-8 -*-
# 14/02/2017 AH - Code to update all hscic.gov.uk emails to nhs.net

# import routine
import sys, json, csv
from collections import defaultdict
import rethinkdb as r

r.connect('localhost', 28015).repl()

people = r.db('workpackages').table('people')
user_data = {}

"""
r.db('workpackages').table('people').filter(function(doc){
  return doc('email').match("@hscic\.gov\.uk$")
})
"""
            
entries_to_change = people.filter(lambda entry: entry['email'].match('@hscic\.gov\.uk$')).run()

for entry in entries_to_change:
    id = entry['id']
    email = entry['email']
    newvalue = email.replace('hscic.gov.uk', 'nhs.net')
    print email + ' -> ' + newvalue
    result = people.get(id).update({"email": newvalue}).run()
    print result
