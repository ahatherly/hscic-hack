# -*- coding: utf-8 -*-
# 12/02/2017 - Adam Hatherly

# Add a new table in RethinDB to hold default value for deliverables
import sys, json, csv
from collections import defaultdict
import rethinkdb as r

if len(argv == 0):
    print "Please provide the hostname of the database as the first parameter to this command"
    exit()

hostname = sys.argv[1]
if hostname == None:
    hostname = 'localhost'

r.connect(hostname, 28015).repl()

try:
    r.db('workpackages').table_create('deliverables').run()
except:
    print 'Table already exists'

deliverables = r.db('workpackages').table('deliverables')

item = {}
item['serviceType'] = 'Solution Architecture'
item['deliverables'] = 'Do solution architecture stuff'
print deliverables.insert(item).run()

item = {}
item['serviceType'] = 'Enterprise Architecture'
item['deliverables'] = 'Do enterprise architecture stuff'
print deliverables.insert(item).run()


deliverables.index_create('serviceType').run()
print "Done"
