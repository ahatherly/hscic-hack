# import routine
import sys, json, csv
from collections import defaultdict
import rethinkdb as r

r.connect('localhost', 28015).repl()

filename = sys.argv[1]

packages = r.db('workpackages').table('packages')

json_data=open(filename).read()
data = json.loads(json_data)

for row in data:
    existing = packages.get(row['id']).run()
    if existing != None:
        print 'Work package already exists: ' + row['name']
    else:
        print packages.insert(row).run()
