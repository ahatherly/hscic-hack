#!/bin/sh
# RethinkDb Nightley backup
backup_date=`date +%Y_%m_%d_%H_%M`
backup_dir="/var/webapp/backups"
cd $backup_dir
. /var/webapp/env/bin/activate
rethinkdb export
