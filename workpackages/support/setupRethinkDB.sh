#/!bin/bash

if [[ $EUID -ne 0 ]]; then
  echo "Must be run as root - run again with: sudo $0"
  exit 1
fi

echo "==== Installing dependencies ===="
apt-get install -y git python-pip python-dev build-essential

echo "==== Installing rethinkdb ===="
add-apt-repository -y ppa:rethinkdb/ppa
apt-get update
apt-get install -y rethinkdb

echo "==== Installing Python Rethinkdb driver ===="
pip install rethinkdb

echo "==== Installing Flask ===="
pip install flask

echo "==== Starting the database ===="
#rethinkdb &
#using service setup now

