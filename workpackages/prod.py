#!/usr/bin/python
from gevent import wsgi
from app import app

wsgi.WSGIServer(('', 8088), app, spawn=None).serve_forever()