"""
Config settings for solution
"""
import os
import sys
from authomatic.providers import oauth2, oauth1, openid
import authomatic

CSRF_ENABLED = True

# Path to secret files for keys etc
sys.path.append('/var/webapp/')
#from secret import github_consumer_key, github_consumer_secret, google_consumer_key, google_consumer_secret, linkedin_consumer_key, linkedin_consumer_secret, SECRET_KEY, test_token, keycloak_consumer_key, keycloak_consumer_secret
from secret import OIDC_CLIENT_SECRETS, OIDC_ID_TOKEN_COOKIE_SECURE, OIDC_VALID_ISSUERS, SECRET_KEY

# Note: Always set this to blank on live boxes!
#TEST_TOKEN = test_token

RDB_HOST =  os.environ.get('RDB_HOST') or 'localhost'
RDB_PORT = os.environ.get('RDB_PORT') or 28015
DB_NAME = 'workpackages'
FIELD_LIST_DISPLAY = ['portfolio', 'name', 'status',
    'areaOfWork', 'id', 'leadPerson']
FIELD_LIST_QUERY = ['portfolio', 'status', 'leadPerson']
FIELD_LIST_DISPLAY_PERSON = ['portfolio', 'name', 'status',
    'areaOfWork', 'id', 'Assigned']

# This is the path which any generated word documents will be saved in
REFERENCE_DOC_PATH = 'app/static/styles.docx'
GENERATED_FILES_PATH = '/var/webapp/tmp/'

# Tolerance for calculating over or under allocation. Will use whichever is the larger (percent or days).
UNDER_ALLOCATION_TOLERANCE_PERCENT = 0.8
UNDER_ALLOCATION_TOLERANCE_DAYS = 2
OVER_ALLOCATION_TOLERANCE_PERCENT = 1.2
OVER_ALLOCATION_TOLERANCE_DAYS = 2

ALLOCATION_COLUMNS_PERSON_PAGE = 5
ALLOCATION_START_OFFSET_PERSON_PAGE = -1

ALLOCATION_COLUMNS_WORKPACKAGE_DETAIL_PAGE = 8
ALLOCATION_START_OFFSET_WORKPACKAGE_DETAIL_PAGE = -1

# The fields in this list will be constrained to only values found in the rethink reference table
CONSTRAINED_FIELDS = ['status','portfolioOwner','portfolio','areaOfWork','serviceType','level1Type','level2Type','dq_filter','personAllocationStatus','originator']

"""
OIDC_CONFIG = { 'OIDC_CLIENT_SECRETS': OIDC_CLIENT_SECRETS,
           'OIDC_ID_TOKEN_COOKIE_SECURE': OIDC_ID_TOKEN_COOKIE_SECURE,
           'OIDC_VALID_ISSUERS': OIDC_VALID_ISSUERS,
           'SECRET_KEY': SECRET_KEY}
"""

OIDC_CONFIG = { 'OIDC_CLIENT_SECRETS': OIDC_CLIENT_SECRETS,
           'OIDC_ID_TOKEN_COOKIE_SECURE': None,
           'OIDC_VALID_ISSUERS': OIDC_VALID_ISSUERS,
           'SECRET_KEY': SECRET_KEY}

"""
CONFIG = {

    'github': {

        'class_': oauth2.GitHub,
        'consumer_key': github_consumer_key,
        'consumer_secret': github_consumer_secret,
        'id': authomatic.provider_id(),
        'scope': oauth2.GitHub.user_info_scope,
    },


    'linkedin': {
        'class_': oauth2.LinkedIn,
        'consumer_key': linkedin_consumer_key,
        'consumer_secret': linkedin_consumer_secret,
        'id': authomatic.provider_id(),
        '_name': 'LinkedIn',
    },

    'google': {
        'class_': oauth2.Google,
        'consumer_key': google_consumer_key,
        'consumer_secret': google_consumer_secret,
        'scope': oauth2.Google.user_info_scope, 
    },
          
    'keycloak': {
        'class_': oauth2.Google,
        'consumer_key': keycloak_consumer_key,
        'consumer_secret': keycloak_consumer_secret,
        'scope': oauth2.Google.user_info_scope, 
    }
}
"""
