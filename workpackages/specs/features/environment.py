"""
Sets up databases for datastore feature
"""
import rethinkdb as r

def before_feature(context, feature):
    """
    Create database and test data
    """
    context.database = r.connect(host=u'localhost', port=28015)
    context.database_name = u'datastorefeature'
    context.feature_table_name = u'datastore'
    

    if context.database_name in r.db_list().run(context.database):
        r.db_drop(context.database_name).run(context.database)

    r.db_create(context.database_name).run(context.database)

    context.database.close()
    context.database = r.connect(host=u'localhost', port=28015,  \
        db=context.database_name)


    r.db(context.database_name) \
        .table_create(context.feature_table_name) \
        .run(context.database)

    context.inserted = r.db(context.database_name) \
        .table(context.feature_table_name) \
        .insert( [
            { 'superhero': 'Wolverine', 'superpower': 'Adamantium' },
            { 'superhero': 'Spiderman', 'superpower': 'spidy sense' }] ) \
        .run(context.database)

    context.inserted_first_id = context.inserted["generated_keys"][0]
    context.inserted_second_id = context.inserted["generated_keys"][1]
    
    
    context.packages_table_name = u'packages'
    
    r.db(context.database_name) \
        .table_create(context.packages_table_name) \
        .run(context.database)
    r.db(context.database_name) \
        .table(context.packages_table_name) \
        .insert( [
                  { "portfolio":"a" ,"name": u"Work expands so as to fill the time available for its completion" },
                  { "portfolio":"b" ,"name": u"A task always takes longer than you expect, even when you take into account Hofstadter's Law" },
                  { "portfolio":"b" ,"name": u"If anything can go wrong, it will."}] ).run(context.database);



def after_feature(context, feature):
    """
    Drop the database
    """
    r.db_drop(context.database_name).run(context.database)
    context.database.close()




