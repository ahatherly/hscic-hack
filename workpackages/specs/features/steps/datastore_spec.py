import sys
from behave import *

sys.path.append('/home/edward/code/hscic-hack/workpackages')

from app.datastore import DataStore

@given("we have a set of documents")
def impl(context):
    pass

@when("we get by known id")
def impl(context):
    store = DataStore(context.database, context.feature_table_name)
    context.doc = store.get(context.inserted_first_id)

@then(u'the correct document is found')
def impl(context):
    assert context.doc["superhero"] == "Wolverine"


@when("we query for single result")
def impl(context):
    store = DataStore(context.database, context.feature_table_name)
    query = store.all().filter( { 'superhero': 'Wolverine' })
    context.doc = store.query_single(query)

@then(u'a single result is returned')
def impl(context):
    assert context.doc["superhero"] == "Wolverine"

@when("we query for results")
def impl(context):
    store = DataStore(context.database, context.feature_table_name)
    query = store.all().filter({ })
    context.docs = store.query(query)

@then("one or more results are returned")
def impl(context):
    assert list(context.docs) > 1

# Posting new document
@given("we have added a new document")
def impl(context):
    store = DataStore(context.database, context.feature_table_name)
    context.key = store.post({ 'superhero': 'Captain America', \
            'superpower': 'Super-Soldier Serum/Vita Ray' })
    assert context.key is not None

@when("we query for new document")
def impl(context):
    store = DataStore(context.database, context.feature_table_name)
    context.doc = store.get(context.key)

@then("the new document is returned")
def impl(context):
    assert context.doc["superhero"] == "Captain America"

@when("we delete new document")
def impl(context):
    store = DataStore(context.database, context.feature_table_name)
    store.delete(context.key)
    
@then(u'the deleted document has audit')
def impl(context):
    store = DataStore(context.database, context.feature_table_name)
    history = store.history(context.key)
    history = list(history)
    assert history is not None
    assert len(history) == 1
    assert history[0]["changelog"][0]["toValue"] == "deleted"


@then("the document is no longer found")
def impl(context):
    store = DataStore(context.database, context.feature_table_name)
    doc = store.get(context.key)
    assert doc is None

@when("we update the document")
def impl(context):
    store = DataStore(context.database, context.feature_table_name)
    context.updated_count = store.put(context.key,  \
        { 'superhero': 'Captain America', \
            'superpower': 'Super-Human' })

@then("the document is updated")
def impl(context):
    assert context.updated_count is not None
    assert context.updated_count == 1

@then(u'a history audit is generated')
def impl(context):
    store = DataStore(context.database, context.feature_table_name)
    context.history = store.history(context.key)
    
    context.history = list(context.history)
    
    assert context.history is not None
    assert len(context.history) == 1

@then(u'a version number exists')
def impl(context):
    assert context.history[0]["version"] == 1
    
@then(u'the document is updated again')
def impl(context):
    store = DataStore(context.database, context.feature_table_name)
    context.updated_count = store.put(context.key,  \
        { 'superhero': 'Captain America', \
            'superpower': 'Super-Human/ freeze tolerant' })
    
@then(u'the version number is incremented')
def impl(context):
    store = DataStore(context.database, context.feature_table_name)
    captain = store.get(context.key)
    assert captain["audit"]["version"] == 3
    
@then(u'the audit history is complete')
def impl(context):
    store = DataStore(context.database, context.feature_table_name)
    history = store.history(context.key)
    history = list(history)
    assert history is not None
    assert len(history) == 2

    
    
    

