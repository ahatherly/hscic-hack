import sys
from behave import *

sys.path.append('/home/edward/code/hscic-hack/workpackages')

from app.datastore import PackagesStore

@given(u'a list of portfolio reference data')
def impl(context):
    pass

@when(u'getting list of portfolios')
def impl(context):
    store = PackagesStore(context.database, context.packages_table_name)
    context.portfolios = store.get_portfolios()
    
@then(u'the portfolio names are returned')
def impl(context):
    print context.portfolios
    assert context.portfolios[0] == "a"
    

@then(u'the portfolios are unique')
def impl(context):
    assert len(context.portfolios) == 2



 
