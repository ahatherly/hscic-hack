Feature: generic datastore

  Scenario: Finding a document
    Given we have a set of documents
     when we get by known id
     then the correct document is found

  Scenario: Filtering documents for single result
    Given we have a set of documents
     when we query for single result
     then a single result is returned

  Scenario: Filtering documents for results
    Given we have a set of documents
     when we query for results
     then one or more results are returned

  Scenario: Posting new document
    Given we have added a new document
     when we query for new document
     then the new document is returned

  Scenario: Deleting document by id
    Given we have added a new document
     when we delete new document
     then the document is no longer found
     then the deleted document has audit

  Scenario: Putting a document
    Given we have added a new document
     when we update the document
     then the document is updated
     
  Scenario: Storing audit of changes
    Given we have added a new document
     when we update the document
     then a history audit is generated
     then a version number exists
     then the document is updated again
     then the version number is incremented
     then the audit history is complete
 