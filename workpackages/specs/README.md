
Prerequisites

1. Ensure that rethinkdb is running (localhost, default port)
2. Run in venv, and install dependencies using `requirements.txt` folder in project root

    pip install -r requirements.txt

3. To run the specs:

    $ behave


