import rethinkdb as r
from app import app
from app.datastores.portfolio import PortfolioStore
import csv, sys
from rethinkdb.errors import RqlDriverError

"""
This class is used to import portfolio items from a CSV file
"""


""" Run the import """
try:
    rdb_conn = r.connect(host=app.config['RDB_HOST'], 
        port=app.config['RDB_PORT'], 
        db=app.config['DB_NAME'])
except RqlDriverError:
    print "No database connection could be established."
    exit(1)

portfolio_dao = PortfolioStore(rdb_conn)

filename = sys.argv[1]

# First, check we have a sensible file with at least 30 programmes in it
good_lines = 0
try:
    with open(filename) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            if row['Portfolio_Code']:
                if len(row['Portfolio_Code']) > 5:
                    good_lines=good_lines+1
    if good_lines < 30:
        print "This file does not appear to have sensible portfolio data in it - aborting"
        exit()
except:
    print "This file does not appear to have sensible portfolio data in it - aborting"
    exit()

# Now. clear out all the data
portfolio_dao.create_table_if_not_exists()
portfolio_dao.clear_existing_programmes()

# And re-import it
with open(filename) as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        #print(row['Portfolio_Code'], row[''])
        portfolio_dao.add_programme(row['Portfolio_Code'], \
                                    row['Portfolio_Item_Name'], \
                                    row['Portfolio_Item_Desc'], \
                                    row['Open_Closed_Status'].lower())
print "Import complete."
