Installing
==========

Install packages:
    sudo apt-get install git python python-virtualenv pandoc python3-genshi

Install rethinkdb:

    source /etc/lsb-release && echo "deb http://download.rethinkdb.com/apt $DISTRIB_CODENAME main" | sudo tee /etc/apt/sources.list.d/rethinkdb.list
    wget -qO- http://download.rethinkdb.com/apt/pubkey.gpg | sudo apt-key add -
    sudo apt-get update
    sudo apt-get install rethinkdb

Setup the DB instance:

    cd /etc/rethinkdb/instances.d
    sudo cp ../default.conf.sample instance1.conf
    sudo vi instance1.conf

Edit instance1.conf and uncomment # http-port=8080

    sudo service rethinkdb start

Test: http://localhost:8080/

Pull latest code:

    mkdir /var/webapp
    cd /var/webapp
    git clone git@bitbucket.org:ahatherly/hscic-hack.git

Setup virtual environment:

    cd /var/webapp
    virtualenv venv
    . venv/bin/activate

Install python libraries:

    pip install -r requirements.txt

Then:

    python run.py

