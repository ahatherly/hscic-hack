#!/usr/bin/python

from app import app
import os

FLASK_HOST_NAME = os.environ.get('FLASK_HOST_NAME') or '0.0.0.0'
FLASK_DEBUG = os.environ.get('FLASK_DEBUG') or True
FLASK_DEBUG=True
app.run(host=FLASK_HOST_NAME, debug = FLASK_DEBUG)
