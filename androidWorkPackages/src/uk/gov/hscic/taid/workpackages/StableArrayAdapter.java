package uk.gov.hscic.taid.workpackages;

import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.widget.ArrayAdapter;

public class StableArrayAdapter extends ArrayAdapter<String> {

    public HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

    public StableArrayAdapter(Context context, int textViewResourceId,
        List<String> objects) {
      super(context, textViewResourceId, objects);
      for (int i = 0; i < objects.size(); ++i) {
        mIdMap.put(objects.get(i), i);
      }
    }
    
    // Add items to the list
    public void addAll(List<String> items) {
    	int idx = mIdMap.size();
    	for (String item : items) {
    		System.out.println("Adding item " + idx + " : " + item);
    		mIdMap.put(item, idx++);
    	}
    	//super.addAll(items);
    }

    @Override
    public long getItemId(int position) {
      System.out.println("Trying to get item at position " + position);
      String item = getItem(position);
      return mIdMap.get(item);
    }

    @Override
    public boolean hasStableIds() {
      return true;
    }
}