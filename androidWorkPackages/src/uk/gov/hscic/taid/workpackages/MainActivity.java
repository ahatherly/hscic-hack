package uk.gov.hscic.taid.workpackages;

import uk.gov.hscic.taid.workpackages.utils.GlobalConstants;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends Activity implements GlobalConstants {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        EditText editText = (EditText) findViewById(R.id.editText1);
        
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        String serverPref = sharedPref.getString("pref_server", DEFAULT_SERVER_ADDRESS);
        editText.setText("URL: " + serverPref);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_settings:
                openSettings();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    
    private void openSettings() {
    	Intent intent = new Intent(this, SettingsActivity.class);
    	startActivity(intent);
    }
    
    /** Called when the user clicks the button */
    public void openMyWorkPackages(View view) {
    	Intent intent = new Intent(this, MyWorkPackagesActivity.class);
    	/*EditText editText = (EditText) findViewById(R.id.edit_message);
    	String message = editText.getText().toString();
    	intent.putExtra(EXTRA_MESSAGE, message);*/
    	startActivity(intent);
    }
    
}
