package uk.gov.hscic.taid.workpackages;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import uk.gov.hscic.taid.workpackages.model.WorkpackageSummaryItem;
import uk.gov.hscic.taid.workpackages.model.WorkpackageSummaryList;

import com.google.gson.Gson;

public class JSONHandler {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String jsonStr = getHTTP("http://192.168.10.103:5000/workpackages/json/");
		//System.out.println("JSON = \n" + jsonStr);
		Gson gson = new Gson();
		WorkpackageSummaryList data = gson.fromJson(jsonStr, WorkpackageSummaryList.class);
		
		for (WorkpackageSummaryItem item : data.list) {
			System.out.println(item.name);
		}
		
	}

	public static String getHTTP(String urlToRead) {
	      URL url;
	      HttpURLConnection conn;
	      BufferedReader rd;
	      String line;
	      StringBuilder result = new StringBuilder();
	      try {
	         url = new URL(urlToRead);
	         conn = (HttpURLConnection) url.openConnection();
	         conn.setRequestMethod("GET");
	         rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	         while ((line = rd.readLine()) != null) {
	            result.append(line);
	         }
	         rd.close();
	      } catch (Exception e) {
	         e.printStackTrace();
	      }
	      return result.toString();
	   }
}
