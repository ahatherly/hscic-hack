package uk.gov.hscic.taid.workpackages;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import uk.gov.hscic.taid.workpackages.model.WorkpackageSummaryItem;
import uk.gov.hscic.taid.workpackages.model.WorkpackageSummaryList;

import com.google.gson.Gson;

import android.widget.EditText;

import android.os.AsyncTask;

public class RequestTask extends AsyncTask<String, String, String>{

	private MyWorkPackagesActivity resultActivity = null;
	
	public RequestTask(MyWorkPackagesActivity resultActivity) {
		this.resultActivity = resultActivity;
	}
	
    @Override
    protected String doInBackground(String... uri) {
        HttpClient httpclient = new DefaultHttpClient();
        HttpResponse response;
        String responseString = null;
        try {
            response = httpclient.execute(new HttpGet(uri[0]));
            StatusLine statusLine = response.getStatusLine();
            if(statusLine.getStatusCode() == HttpStatus.SC_OK){
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                response.getEntity().writeTo(out);
                out.close();
                responseString = out.toString();
            } else{
                //Closes the connection.
                response.getEntity().getContent().close();
                throw new IOException(statusLine.getReasonPhrase());
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
        	e.printStackTrace();
        }
        return responseString;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        System.out.println("onPostExecute");
        Gson gson = new Gson();
		WorkpackageSummaryList data = gson.fromJson(result, WorkpackageSummaryList.class);
		
		ArrayList<String> wpNames = new ArrayList<String>();
		for (WorkpackageSummaryItem item : data.list) {
			wpNames.add(item.name);
			System.out.println(item.name);
		}
		System.out.println("attempting to update");
		resultActivity.updateListData(wpNames);
    }
}