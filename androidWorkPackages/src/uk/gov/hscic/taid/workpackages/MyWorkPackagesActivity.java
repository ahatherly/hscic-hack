package uk.gov.hscic.taid.workpackages;

import java.util.ArrayList;

import uk.gov.hscic.taid.workpackages.model.WorkpackageSummaryList;
import uk.gov.hscic.taid.workpackages.utils.GlobalConstants;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class MyWorkPackagesActivity extends Activity implements GlobalConstants {

	private StableArrayAdapter listAdapter = null;
	
	public void updateListData(ArrayList<String> listData) {
		System.out.println("updateListData");
		//this.listAdapter.mIdMap.clear();
		this.listAdapter.addAll(listData);
		System.out.println("NotifyChanged");
		listAdapter.notifyDataSetChanged();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_my_work_packages);
		
		// Get the URL of the server
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        String serverPref = sharedPref.getString("pref_server", DEFAULT_SERVER_ADDRESS);
		
		// Trigger HTTP call to get data
		RequestTask asyncGetter = new RequestTask(this);
		asyncGetter.execute(serverPref + "workpackages/json/");
		
		// Create a list - the data items will be overwritten once
		// the http call returns its results
		final ListView listview = (ListView) findViewById(R.id.listView1);
	    String[] values = new String[] { "Loading..." };

	    final ArrayList<String> listData = new ArrayList<String>();
	    for (int i = 0; i < values.length; ++i) {
	    	listData.add(values[i]);
	    }
	    listAdapter = new StableArrayAdapter(this,
	        android.R.layout.simple_list_item_1, listData);
	    listview.setAdapter(listAdapter);
	    
	    // When item clicked
	    listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
	        @SuppressLint("NewApi")
			@Override
	        public void onItemClick(AdapterView<?> parent, final View view,
	            int position, long id) {
	          final String item = (String) parent.getItemAtPosition(position);
	          
	          
	          int version = android.os.Build.VERSION.SDK_INT;
	          if (version >= 12) {
		          view.animate().setDuration(2000).alpha(0)
		              .withEndAction(new Runnable() {
		                @Override
		                public void run() {
		                	listData.remove(item);
		                	listAdapter.notifyDataSetChanged();
		                	view.setAlpha(1);
		                }
		              });
	          } else {
	        	  listData.remove(item);
	        	  listAdapter.notifyDataSetChanged();
	          }
	        }

	      });
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.my_work_packages, menu);
		return true;
	}

}
