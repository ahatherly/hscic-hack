package uk.gov.hscic.taid.workpackages.model;

public class WorkpackageSummaryItem {
	public String areaOfWork;
    public String id; 
    public String leadPerson; 
    public String name; 
    public String portfolio; 
    public String portfolioOwner; 
    public String status;

    public WorkpackageSummaryItem() {
    }
}
